﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NngLevelEditor.Entities;
using NngLevelEditor.Manager;

namespace NngLevelEditor
{
    public partial class Editor : Form
    {
        public enum EditorState
        {
            Insert,
            Edit,
            Move
        }

        public static EditorState State
        {
            get;
            set;
        }

        public Editor()
        {
            InitializeComponent();
            State = EditorState.Insert;
        }

        #region Events
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void canvas_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (State == EditorState.Insert)
                {
                    canvas.PlaceObject(e.X, e.Y);
                }
                else if (State == EditorState.Edit)
                {
                    canvas.SelectObject(e.X, e.Y);
                    properties1.SetSelectedItem(SelectionManager.SelectedEntity);
                }
                else if (State == EditorState.Move)
                {
                    if (SelectionManager.SelectedEntity != null)
                    {
                        canvas.MoveObject(e.X, e.Y);
                        properties1.SetSelectedItem(SelectionManager.SelectedEntity);
                    }
                }
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (State == EditorState.Insert)
                {
                    canvas.RemoveObject(e.X, e.Y);
                    properties1.SetSelectedItem(SelectionManager.SelectedEntity);
                }
                else if (State == EditorState.Edit)
                {
                    canvas.RemoveObject(e.X, e.Y);
                    properties1.SetSelectedItem(SelectionManager.SelectedEntity);
                }
                else if (State == EditorState.Move)
                {
                    if (SelectionManager.SelectedEntity != null)
                    {
                        canvas.MoveOtherObject(e.X, e.Y);
                        properties1.SetSelectedItem(SelectionManager.SelectedEntity);
                    }
                }
            }
        }

        private void Editor_Load(object sender, EventArgs e)
        {
            RefreshControls();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure you would like to exit this level and create a new one?",
                                         "New Level", MessageBoxButtons.YesNo, MessageBoxIcon.None);
            if (result == DialogResult.Yes)
            {
                SelectionManager.Level.New();
                RefreshControls();
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Xzipit Level|*.xlvl";
            open.Title = "Open a Level";
            open.AddExtension = true;
            open.ShowDialog();

            SelectionManager.Level.SetPath(open.FileName);
            SelectionManager.Level.Open();
            RefreshControls();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectionManager.Level.GetPath().Equals(""))
            {
                SaveAs();
            }
            SelectionManager.Level.Save();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAs();
            SelectionManager.Level.Save();
        }

        #endregion

        private void SaveAs()
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Xzipit Level|*.xlvl";
            save.Title = "Save a Level";
            save.AddExtension = true;
            save.ShowDialog();

            if (!save.FileName.Equals(""))
            {
                SelectionManager.Level.SetPath(save.FileName);
            }
        }

        private void RefreshControls()
        {
            levelProperties1.SetSelectedItem(SelectionManager.Level);
            properties1.SetSelectedItem(null);
        }
    }
}
