﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NngLevelEditor.Utility;
using System.ComponentModel;
using NngLevelEditor.Manager;

namespace NngLevelEditor.Entities
{
    public abstract class SpriteEntity : Entity
    {
        protected float _scale;
        protected bool _visible = true;
        protected int _type;
        protected string _path;

        protected Sprite Sprite
        {
            get;
            set;
        }

        public override Vector2 Position
        {
            get { return base.Position; }
            set
            {
                base.Position = value;
                Sprite.Position = value;
            }
        }

        public override float Rotation
        {
            get { return base.Rotation; }
            set
            {
                base.Rotation = value;
                Sprite.Rotation = value;
            }
        }

        [DescriptionAttribute("Whether this entity is visible or not."),
        ReadOnlyAttribute(true)]
        public virtual bool Visible
        {
            get { return _visible; }
            set {             
                _visible = value;
                Sprite.Visible = value;
            }
        }

        [DescriptionAttribute("The scale of this entity.")]
        public virtual float Scale
        {
            get { return _scale; }
            set
            {
                _scale = value;
                Sprite.Scale = value;
            }
        }

        [DescriptionAttribute("The numbered image the button will be.")]
        public int Type
        {
            get { return _type; }
            set
            {
                _type = value;
                OnTypeChange();
            }
        }

        public override void LoadContent()
        {
            base.LoadContent();

            Sprite = GraphicsManager.AddSprite(_path + _type, "Content");
            Sprite.Scale = 1.0f;
        }

        public virtual void UnloadContent()
        {
            GraphicsManager.RemoveSprite(Sprite);
            Sprite = null;
        }

        protected virtual void OnTypeChange()
        {
            GraphicsManager.RemoveSprite(Sprite);
            try
            {
                Sprite = GraphicsManager.AddSprite(_path + _type, "Content");
                Sprite.Position = this.Position;
            }
            catch
            {
                Sprite = new Sprite();
            }
        }

    }
}
