﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NngLevelEditor.Manager;

namespace NngLevelEditor.Entities
{
    public class Velcro : SpriteEntity
    {
        public Velcro()
        {
            _type = 1;
            //LoadContent();
        }

        public Velcro(int type)
        {
            _type = type;
            //LoadContent();
        }

        public Velcro(string[] info)
        {
            _type = int.Parse(info[3]);

            LoadContent();

            // set the position after loading the content because it sets the Sprite's position.
            Position = new Microsoft.Xna.Framework.Vector2(float.Parse(info[1]), float.Parse(info[2]));
            Rotation = float.Parse(info[4]);
        }

        public override void LoadContent()
        {
            _path = "entities/velcro/";

            base.LoadContent();
        }

        public override string GetFileString()
        {
            string fileString = "v " + _position.X + " " + _position.Y + " "
                              + _type + " " + Rotation;
            return fileString + base.GetFileString();
        }
    }
}
