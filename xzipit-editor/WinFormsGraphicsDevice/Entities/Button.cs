﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NngLevelEditor.Manager;
using Microsoft.Xna.Framework;
using System.ComponentModel;
using NngLevelEditor.Utility;

namespace NngLevelEditor.Entities
{
    public class Button : SpriteEntity
    {
        protected Sprite _endSprite;
        protected Vector2 _endPosition;

        [DescriptionAttribute("The location the button will move to when activated.")]
        public Vector2 EndPosition
        {
            get { return _endPosition; }
            set
            {
                _endPosition = value;
                _endSprite.Position = value;
            }
        }

        public Button()
        {
            _type = 1;
            //LoadContent();
        }

        public Button(int type)
        {
            _type = type;
            //LoadContent();
        }

        public Button(string[] info)
        {
            _type = int.Parse(info[3]);

            LoadContent();

            // set the position after loading the content because it sets the Sprite's position.
            Position = new Microsoft.Xna.Framework.Vector2(float.Parse(info[1]), float.Parse(info[2]));
            EndPosition = new Vector2(float.Parse(info[4]), float.Parse(info[5]));
        }

        public override void LoadContent()
        {
            _path = "entities/button/";

            _endPosition = new Vector2();
            _endSprite = GraphicsManager.AddSprite(_path + "end");
            _endSprite.Position = EndPosition;
            _endSprite.Visible = false;

            base.LoadContent();
        }

        public override string GetFileString()
        {
            string fileString = "b " + _position.X + " " + _position.Y + " " 
                              + _type + " " + EndPosition.X + " " + EndPosition.Y;
            return fileString + base.GetFileString();
        }

        public override void Select()
        {
            base.Select();
            _endSprite.Visible = true;
        }

        public override void Deselect()
        {
            base.Deselect();
            _endSprite.Visible = false;
        }
    }
}
