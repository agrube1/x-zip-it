﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using NngLevelEditor.Utility;
using Microsoft.Xna.Framework.Content;
using NngLevelEditor.Manager;

namespace NngLevelEditor.Entities
{
    public class TrackNode : SpriteEntity
    {
        public TrackNode()
        {
            //LoadContent();
        }

        public TrackNode(int type)
        {
            _type = type;
            //LoadContent();
        }

        public TrackNode(string[] info)
        {
            LoadContent();

            Position = new Microsoft.Xna.Framework.Vector2(float.Parse(info[1]), float.Parse(info[2]));
        }

        public override void LoadContent()
        {
            _path = "entities/node/";
            _type = 1;

            base.LoadContent();
        }
    }
}
