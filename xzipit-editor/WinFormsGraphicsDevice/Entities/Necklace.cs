﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NngLevelEditor.Manager;
using Microsoft.Xna.Framework;
using System.ComponentModel;
using NngLevelEditor.Utility;

namespace NngLevelEditor.Entities
{
    public class Necklace : SpriteEntity
    {
        protected Sprite _altSprite;
        protected Vector2 _altPosition;

        [DescriptionAttribute("The location the necklace will move to when activated.")]
        public Vector2 AltPosition
        {
            get { return _altPosition; }
            set
            {
                _altPosition = value;
                _altSprite.Position = value;
            }
        }

        public Necklace()
        {
            _type = 1;         
            //LoadContent();
        }

        public Necklace(int type)
        {
            _type = type;
            //LoadContent();
        }

        public Necklace(string[] info)
        {
            _type = int.Parse(info[3]);

            LoadContent();

            // set the position after loading the content because it sets the Sprite's position.
            Position = new Microsoft.Xna.Framework.Vector2(float.Parse(info[1]), float.Parse(info[2]));
            AltPosition = new Vector2(float.Parse(info[4]), float.Parse(info[5]));
        }

        public override void LoadContent()
        {
            _path = "entities/necklace/";

            _altPosition = new Vector2();
            _altSprite = GraphicsManager.AddSprite(_path + "end");
            _altSprite.Position = AltPosition;
            _altSprite.Visible = false;

            base.LoadContent();
        }

        public override string GetFileString()
        {
            string fileString = "o " + _position.X + " " + _position.Y + " "
                              + _type + " " + AltPosition.X + " " + AltPosition.Y;
            return fileString + base.GetFileString();
        }

        public override void Select()
        {
            base.Select();
            _altSprite.Visible = true;
        }

        public override void Deselect()
        {
            base.Deselect();
            _altSprite.Visible = false;
        }
    }
}
