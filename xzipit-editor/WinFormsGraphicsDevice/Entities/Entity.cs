﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using NngLevelEditor.Utility;
using System.ComponentModel;
using NngLevelEditor.Manager;

namespace NngLevelEditor.Entities
{
    public abstract class Entity
    {
        protected Vector2 _position;
        protected float _rotation;

        protected Sprite _selection;

        [DescriptionAttribute("The position of this entity.")]
        public virtual Vector2 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                _selection.Position = value;
            }
                  
        }

        [DescriptionAttribute("The Z rotation of this entity.")]
        public virtual float Rotation
        {
            get { return _rotation; }
            set
            {
                _rotation = value;
                _selection.Rotation = value;
            }
        }

        public virtual void LoadContent()
        {
            _position = new Vector2();

            _selection = GraphicsManager.AddSprite("entities/selection");
            _selection.Visible = false;
            _selection.Position = _position;
        }

        public virtual String GetFileString()
        {
            return "\n";
        }

        public virtual void Select()
        {
            _selection.Visible = true;
        }

        public virtual void Deselect()
        {
            _selection.Visible = false;
        }
     }
}
