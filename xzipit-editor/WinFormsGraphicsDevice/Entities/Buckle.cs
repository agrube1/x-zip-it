﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NngLevelEditor.Manager;
using System.ComponentModel;

namespace NngLevelEditor.Entities
{
    public class Buckle : SpriteEntity
    {
        private int _countdown;

        [DescriptionAttribute("The time it takes for the buckle to close after opening. 0 signifies it doesn't close.")]
        public int Countdown
        {
            get { return _countdown; }
            set { _countdown = value; }
        }

        public Buckle()
        {
            _type = 1;
           //LoadContent();
        }

        public Buckle(int type)
        {
            _type = type;
            //LoadContent();
        }

        public Buckle(string[] info)
        {
            _type = int.Parse(info[3]);

            LoadContent();

            _countdown = int.Parse(info[5]);

            // set the position after loading the content because it sets the Sprite's position.
            Position = new Microsoft.Xna.Framework.Vector2(float.Parse(info[1]), float.Parse(info[2]));
            Rotation = float.Parse(info[4]);
        }

        public override void LoadContent()
        {
            _countdown = 0;
            _path = "entities/buckle/";

            base.LoadContent();
        }

        public override string GetFileString()
        {
            string fileString = "k " + _position.X + " " + _position.Y + " "
                              + _type + " " + Rotation + " " + _countdown;
            return fileString + base.GetFileString();
        }
    }
}
