﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using NngLevelEditor.Utility;
using NngLevelEditor.Manager;

namespace NngLevelEditor.Entities
{
    public class Track : Entity
    {
        private List<Vector2> _ctrlPts;
        private List<Vector2> _dCtrlPts;
        private List<Sprite> _tracks;
        private List<TrackNode> _track;
        private List<float> _us;

        private ContentManager _manager;

        public List<Vector2> ControlPoints
        {
            get { return _ctrlPts; }
        }

        public Track()
        {
            _tracks = new List<Sprite>();
            _us = new List<float>();
            _track = new List<TrackNode>();
        }

        #region Construct Track

        private void UpdateTrack()
        {

        }

        #endregion

        #region Track Management

        public void Add(TrackNode tn)
        {
            _track.Add(tn);
            if (_track.Count > 1)
                UpdateTrack();
        }

        public bool MoveUp(TrackNode tn)
        {
            bool result = false;

            try
            {
                int index = _track.IndexOf(tn);

                if (index <= 0)
                {
                    result = false;
                }
                else
                {
                    _track.Remove(tn);
                    _track.Insert(index - 1, tn);
                    result = true;
                    UpdateTrack();
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public bool MoveDown(TrackNode tn)
        {
            bool result = false;

            try
            {
                int index = _track.IndexOf(tn);

                if (index >= _track.Count - 1)
                {
                    result = false;
                }
                else
                {
                    _track.Remove(tn);
                    _track.Insert(index + 1, tn);
                    result = true;
                    UpdateTrack();
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public bool Remove(TrackNode tn)
        {
            try
            {
                _track.Remove(tn);
                ((TrackNode)tn).UnloadContent();
                UpdateTrack();
                return true;
            }
            catch
            {

            }

            return false;
        }

        public void Clear()
        {
            try
            {
                foreach (TrackNode tn in _track)
                {
                    tn.UnloadContent();
                }

                _track.Clear();
            }
            catch
            {

            }
        }

        public override string GetFileString()
        {
            string fileString = "track-\n";

            foreach (TrackNode t in _track)
            {
                fileString += "n " + t.Position.X + " " + t.Position.Y + "\n";
            }
            return fileString + "-track\n";
        }
        #endregion

        public bool DetectSelection(Vector2 clickPosition)
        {
            Vector2 selectionSize = new Vector2(32.0f, 32.0f);
            selectionSize = selectionSize.ToWorldUnits();

            foreach (Entity e in _track)
            {
                Vector2 deltaPosition = e.Position - clickPosition;
                if (Math.Abs(deltaPosition.X) < (selectionSize.X) && Math.Abs(deltaPosition.Y) < (selectionSize.X))
                {
                    SelectionManager.SelectedEntity = e;
                    return true;
                }
            }

            return false;
        }

        //public void Draw(SpriteBatch sb)
        //{
        //    foreach (Sprite s in _tracks)
        //    {
        //        s.Draw(sb);
        //    }
        //}

    }
}
