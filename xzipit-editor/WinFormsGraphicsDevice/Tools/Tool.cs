﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NngLevelEditor.Entities;

namespace NngLevelEditor.Tools
{
    public class Tool
    {
        private String _name;

        private int _number;

        public Tool(string name)
        {
            _name = name;
        }

        public Tool(string name, int number)
        {
            _name = name;
            _number = number;
        }

        public Entity getEntity()
        {
            if (_name.Equals("TrackNode"))
            {
                TrackNode t = new TrackNode(_number);
                t.LoadContent();

                return t;
            }
            else if (_name.Equals("Button"))
            {
                Button b = new Button(_number);
                b.LoadContent();

                return b;
            }
            else if (_name.Equals("Buckle"))
            {
                Buckle b = new Buckle(_number);
                b.LoadContent();

                return b;
            }
            else if (_name.Equals("Velcro"))
            {
                Velcro v = new Velcro(_number);
                v.LoadContent();

                return v;
            }
            else if (_name.Equals("Necklace"))
            {
                Necklace n = new Necklace(_number);
                n.LoadContent();

                return n;
            }
            else
            {
                return null;
            }
        }

        public string ToString()
        {
            return _name;
        }

    }
}
