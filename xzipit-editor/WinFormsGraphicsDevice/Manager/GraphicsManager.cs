﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NngLevelEditor.Utility;
using Microsoft.Xna.Framework.Graphics;

namespace NngLevelEditor.Manager
{
    public static class GraphicsManager
    {
        public enum Layer
        {
            Foreground,
            Ground,
            Background
        }

        //private static List<Sprite> _fg = new List<Sprite>();
        private static List<Sprite> _sprites = new List<Sprite>();
        private static List<Sprite> _bg = new List<Sprite>();
        private static Dictionary<String, Texture2D> _textures = new Dictionary<String, Texture2D>();
        private static SpriteBatch _spriteBatch;
        private static Camera _camera;

        private static GraphicsDevice _graphics;

        public static Camera Camera
        {
            get { return _camera; }
        }

        public static float ViewHeight
        {
            get { return _graphics.Viewport.Height; }
        }

        public static float ViewWidth
        {
            get { return _graphics.Viewport.Width; }
        }

        public static void Initialize(GraphicsDevice gd)
        {
            _graphics = gd;
            _spriteBatch = new SpriteBatch(gd);
            _camera = new Camera();
        }

        public static Sprite AddSprite(string asset)
        {
            Sprite s = new Sprite();
            s.LoadContent(Content.GetContentManager("Content"), asset);
            _sprites.Add(s);
            return s;
        }

        public static Sprite AddSprite(string asset, string contentManager, Layer l)
        {
            Sprite s = new Sprite();
            if (_textures.ContainsKey(asset))
            {
                Texture2D savedTexture;
                _textures.TryGetValue(asset, out savedTexture);
                s.LoadContent(savedTexture);
            }
            else
            {
                try
                {
                    Texture2D newTexture;
                    newTexture = Content.GetContentManager(contentManager).Load<Texture2D>(asset);
                    s.LoadContent(newTexture);
                    _textures.Add(asset, newTexture);
                }
                catch
                {
                    return null;
                }
            }
            if (l == Layer.Background)
            {
                _bg.Add(s);
            }
            else if (l == Layer.Ground)
            {
                _sprites.Add(s);
            }
            else if (l == Layer.Foreground)
            {
                //_fg.Add(s);
            }
            return s;
        }

        public static Sprite AddSprite(string asset, string contentManager)
        {
            Sprite s = new Sprite();
            if (_textures.ContainsKey(asset))
            {
                Texture2D savedTexture;
                _textures.TryGetValue(asset, out savedTexture);
                s.LoadContent(savedTexture);
            }
            else
            {
                try
                {
                    Texture2D newTexture;
                    newTexture = Content.GetContentManager(contentManager).Load<Texture2D>(asset);
                    s.LoadContent(newTexture);
                    _textures.Add(asset, newTexture);
                }
                catch
                {
                    return null;
                }
            }
            _sprites.Add(s);
            return s;
        }

        public static void RemoveSprite(Sprite s)
        {
            _bg.Remove(s);
            _sprites.Remove(s);
            s.UnloadContent();
            //_fg.Remove(s);
        }

        public static void Draw()
        {
            _spriteBatch.Begin(SpriteSortMode.Deferred,
                                BlendState.AlphaBlend,
                                null,
                                null,
                                null,
                                null,
                                _camera.GetViewMatrix(_graphics));
            foreach (Sprite sp in _bg)
            {
                sp.Draw(_spriteBatch);
            }
            foreach (Sprite s in _sprites)
            {
                s.Draw(_spriteBatch);
            }
            //foreach (Sprite sy in _fg)
            //{
            //    sy.Draw(_spriteBatch);
            //}
            _spriteBatch.End();
        }
    }
}
