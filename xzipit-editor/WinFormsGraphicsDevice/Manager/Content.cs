﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;

namespace NngLevelEditor.Manager
{
    public static class Content
    {
        private static Dictionary<string, ContentManager> _managers = new Dictionary<string, ContentManager>();

        public static void AddContentManager(string name, ContentManager cm)
        {
            _managers.Add(name, cm);
        }

        public static ContentManager GetContentManager(string name)
        {
            ContentManager cm;
            try
            {
                _managers.TryGetValue(name, out cm);
            }
            catch
            {
                cm = null;
            }

            return cm;
        }

        public static bool RemoveContentManager(string name)
        {
            ContentManager contentManager;
            try
            {
                _managers.TryGetValue(name, out contentManager);
                contentManager.Unload();
                _managers.Remove(name);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
