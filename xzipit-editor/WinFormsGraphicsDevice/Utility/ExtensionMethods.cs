﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using NngLevelEditor.Manager;
using NngLevelEditor.Entities;

namespace NngLevelEditor.Utility
{
    public static class ExtensionMethods
    {
        public static Vector2 ToWorldUnits(this Vector2 v)
        {
            Vector2 w = new Vector2();
            w.X = v.X / (GraphicsManager.ViewWidth / 2.0f) * 10.0f;
            w.Y = -v.Y / (GraphicsManager.ViewHeight / 2.0f) * 16.0f;

            return w;
        }

        public static Vector2 ToXnaUnits(this Vector2 v)
        {
            Vector2 w = new Vector2();
            w.X = v.X * (GraphicsManager.ViewWidth / 2.0f) / 10.0f;
            w.Y = -v.Y * (GraphicsManager.ViewHeight / 2.0f) / 16.0f;

            return w;
        }

        public static Vector2 ToCameraUnits(this Vector2 v)
        {
            Vector2 realPos = v;
            Vector2 translate = new Vector2();
            translate.X = GraphicsManager.ViewWidth / 2.0f;
            translate.Y = GraphicsManager.ViewHeight / 2.0f;
            realPos -= translate;

            return realPos;
        }

        public static string GetFileString(this List<Entity> eList)
        {
            string fileString = "";
            foreach (Entity e in eList)
            {
                fileString += e.GetFileString();
            }

            return fileString;
        }

    }
}
