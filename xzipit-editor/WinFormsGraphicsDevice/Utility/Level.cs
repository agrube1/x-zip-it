﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NngLevelEditor.Entities;
using Microsoft.Xna.Framework;
using NngLevelEditor.Manager;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace NngLevelEditor.Utility
{
    public class Level
    {
        private List<Entity> _entities;
        private Track _track;
        private Sprite _background;
        private int _bg;

        private String _path;

        #region Properties

        [CategoryAttribute("Level Strings"), DescriptionAttribute("The name of the level.")]
        public string LevelName
        {
            get;
            set;
        }

        [CategoryAttribute("Level Strings"), DescriptionAttribute("The description text of this level.")]
        public string LevelDescription
        {
            get;
            set;
        }

        [CategoryAttribute("Time"), DescriptionAttribute("The time to acquire a gold pin.")]
        public float GoldTime
        {
            get;
            set;
        }

        [CategoryAttribute("Time"), DescriptionAttribute("The time to acquire a silver pin.")]

        public float SilverTime
        {
            get;
            set;
        }

        [CategoryAttribute("Time"), DescriptionAttribute("The time to acquire a bronze pin.")]
        public float BronzeTime
        {
            get;
            set;
        }

        [DescriptionAttribute("Whether this level is a tutorial level or not.")]
        public bool TutorialLevel
        {
            get;
            set;
        }

        public int Background
        {
            get { return _bg; }
            set
            {
                _bg = value;
                OnBackgroundChange();
            }
        }

        #endregion

        public Level()
        {
            _entities = new List<Entity>();
            _track = new Track();
            _path = null;
        }

        // to load a level
        public Level(string levelfile)
        {
            _entities = new List<Entity>();
            _track = new Track();
            _path = levelfile;
        }

        private void Initialize()
        {
            if (_path.Equals(""))
            {
                _background = new Sprite();
                _background.Position = Vector2.Zero; 
                _bg = 0;
            }
        }

        public void AddEntity(Entity e)
        {
            _entities.Add(e);
        }

        public void RemoveEntity(Entity e)
        {
            _entities.Remove(e);
            ((SpriteEntity)e).UnloadContent();
        }

        public void AddTrackNode(Entity t)
        {
            try
            {
                _track.Add((TrackNode)t);
            }
            catch
            {

            }
        }

        public void RemoveTrackNode(Entity t)
        {
            try
            {
                _track.Remove((TrackNode)t);
            }
            catch
            {

            }
        }

        /// <summary>
        /// Selects an entity in edit mode.
        /// </summary>
        /// <param name="clickPosition">the position of the click</param>
        /// <returns>The position of the Selected Entity</returns>
        public Vector2? SelectEntity(Vector2 clickPosition)
        {
            if (SelectionManager.SelectedEntity != null)
            {
                SelectionManager.SelectedEntity.Deselect();
            }
            Vector2 selectionSize = new Vector2(32.0f, 32.0f);
            selectionSize = selectionSize.ToWorldUnits();
            try
            {
                if (!_track.DetectSelection(clickPosition))
                {
                    foreach (Entity e in _entities)
                    {
                        Vector2 deltaPosition = e.Position - clickPosition;
                        if (Math.Abs(deltaPosition.X) < (selectionSize.X) && Math.Abs(deltaPosition.Y) < (selectionSize.X))
                        {
                            SelectionManager.SelectedEntity = e;
                            e.Select();
                            return e.Position;
                        }
                    }
                }
                else
                {
                    return SelectionManager.SelectedEntity.Position;
                }
                //SelectionManager.SelectedEntity = null;
            }
            catch
            {
                SelectionManager.SelectedEntity.Deselect();
                SelectionManager.SelectedEntity = null;
            }

            return null;
        }

        protected void Clear()
        {
            try
            {
                LevelName = String.Empty;
                LevelDescription = String.Empty;
                GoldTime = 0;
                SilverTime = 0;
                BronzeTime = 0;
                TutorialLevel = false;
                foreach (Entity e in _entities)
                {
                    ((SpriteEntity)e).UnloadContent();
                }

                _entities.Clear();

                _track.Clear();
                GraphicsManager.RemoveSprite(_background);
            }
            catch
            {

            }
        }

        private void OnBackgroundChange()
        {
            if (_background != null)
            {
                GraphicsManager.RemoveSprite(_background);
            }

            try
            {
                _background = GraphicsManager.AddSprite("bg/" + _bg, "Content", GraphicsManager.Layer.Background);
                _background.Position = Vector2.Zero;
            }
            catch
            {
                _background = new Sprite();
            }
            
        }

        public string CreateSaveString()
        {
            string saveString = "ln " + LevelName + "\n"
                              + "ld " + LevelDescription + "\n"
                              + "t g " + GoldTime + "\n"
                              + "t s " + SilverTime + "\n"
                              + "t b " + BronzeTime + "\n"
                              + "tut " + (TutorialLevel ? "y" : "n") + "\n"
                              + "l-" + "\n"
                              + "bg " + _bg + "\n"
                              + _track.GetFileString()
                              + _entities.GetFileString()
                              + "-l\n";

            return saveString;
        }

        public void SetPath(string path)
        {
            _path = path;
        }

        public string GetPath()
        {
            return _path;
        }

        public void Save()
        {
            if (_path.Length > 0)
            {
                try
                {
                    StreamWriter write = new StreamWriter(_path, false);
                    write.WriteLine(CreateSaveString());
                    write.Close();

                    MessageBox.Show(_path + " saved successfully!");
                }
                catch
                {
                    MessageBox.Show("Save Unsuccessful!");
                }
            }

        }

        public void Open()
        {
            if (_path.Length > 0)
            {
                this.Clear();

                StreamReader read = new StreamReader(_path, false);
                List<Entity> go = new List<Entity>();
                Track t = new Track();
                while (!read.EndOfStream)
                {
                    string[] line = read.ReadLine().Split(' ');
                    if (line[0].Equals("bg"))
                    {
                        Background = int.Parse(line[1]);
                    }
                    else if (line[0].Equals("tut"))
                    {
                        if (line[1].Equals("y"))
                            TutorialLevel = true;
                        else
                            TutorialLevel = false;
                    }
                    else if (line[0].Equals("n"))
                    {
                        t.Add(new TrackNode(line));
                    }
                    else if (line[0].Equals("b"))
                    {
                        // change the last ones to line[1], line[2] to load brandon's levels.
                        go.Add(new NngLevelEditor.Entities.Button(line));
                    }
                    else if (line[0].Equals("v"))
                    {
                        go.Add(new Velcro(line));
                    }
                    else if (line[0].Equals("o"))
                    {
                        go.Add(new Necklace(line));
                    }
                    else if (line[0].Equals("k"))
                    {
                        go.Add(new Buckle(line));
                    }
                    else if (line[0].Equals("t"))
                    {
                        if (line[1].Equals("g"))
                        {
                            GoldTime = float.Parse(line[2]);
                        }
                        if (line[1].Equals("s"))
                        {
                            SilverTime = float.Parse(line[2]);
                        }
                        if (line[1].Equals("b"))
                        {
                            BronzeTime = float.Parse(line[2]);
                        }
                    }
                    else if (line[0].Equals("ln"))
                    {
                        LevelName = "";
                        for (int i = 1; i < line.Length; i++)
                        {
                            LevelName += line[i] + " ";
                        }
                        LevelName.Trim();
                    }
                    else if (line[0].Equals("ld"))
                    {
                        LevelDescription = "";
                        for (int i = 1; i < line.Length; i++)
                        {
                            LevelDescription += line[i] + " ";
                        }
                        LevelDescription.Trim();
                    }
                }

                _entities = go;
                _track = t;

                read.Close();

            }
        }

        public void New()
        {
            Clear();

            _path = "";
        }
    }
}
