﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using NngLevelEditor.Entities;
using Microsoft.Xna.Framework.Graphics;

namespace NngLevelEditor.Utility
{
    public class Camera : Entity
    {
        protected float _zoom;
        protected Matrix _viewMatrix;

        public float Zoom
        {
            get { return _zoom; }
            set
            {
                _zoom = value;
                if (_zoom < 0.0f) _zoom = 0.0f;
            }
        }

        public Camera()
        {
            _zoom = 1.0f;
            _rotation = 0.0f;
            _position = Vector2.Zero;
        }

        public Camera(Vector2 position)
        {
            _zoom = 1.0f;
            _rotation = 0.0f;
            _position = position;
        }

        public Matrix GetViewMatrix(GraphicsDevice graphics)
        {
            _viewMatrix = Matrix.CreateTranslation(
                                    new Vector3(-_position.X, -_position.Y, 0)) *
                                                Matrix.CreateRotationZ(_rotation) *
                                                Matrix.CreateScale(new Vector3(_zoom, _zoom, 1)) *
                                                Matrix.CreateTranslation(new Vector3(graphics.Viewport.Width * 0.5f,
                                                                                     graphics.Viewport.Height * 0.5f,
                                                                                     0));

            return _viewMatrix;
        }
    }
}
