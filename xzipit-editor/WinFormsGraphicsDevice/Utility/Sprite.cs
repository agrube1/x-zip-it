﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using NngLevelEditor.Manager;

namespace NngLevelEditor.Utility
{
    public class Sprite
    {
        private Texture2D _texture;
        private Vector2 _position;

        public Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public float Rotation
        {
            get;
            set;
        }

        public float Scale
        {
            get;
            set;
        }

        public bool Visible
        {
            get;
            set;
        }

        public void LoadContent(ContentManager manager, String asset)
        {
            _texture = manager.Load<Texture2D>(asset);
            Scale = 1.0f;
            Visible = true;
        }

        public void LoadContent(Texture2D texture)
        {
            _texture = texture;
            Scale = 1.0f;
            Visible = true;
        }

        public void UnloadContent()
        {
            _texture = null;
        }

        public void Draw(SpriteBatch sb)
        {
            if (Visible)
            {
                Vector2 drawPos = Position.ToXnaUnits();
                sb.Draw(_texture, drawPos, null, Color.White,
                        -Rotation, new Vector2(_texture.Width / 2, _texture.Height / 2),
                        Scale, SpriteEffects.None, 0.0f);
            }
        }

        public void RotateTowards(Vector2 pt)
        {
            float faceAtX = pt.X;
            float faceAtY = pt.Y;

            float rotation = (float)Math.Atan2(faceAtY, faceAtX) - (float)Math.PI / 2.0f;

            this.Rotation = rotation;
        }
    }
}
