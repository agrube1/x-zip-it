﻿namespace NngLevelEditor.Controls
{
    partial class SideBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SideBar));
            this.insert = new System.Windows.Forms.PictureBox();
            this.edit = new System.Windows.Forms.PictureBox();
            this.move = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.insert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.move)).BeginInit();
            this.SuspendLayout();
            // 
            // insert
            // 
            this.insert.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.insert.BackColor = System.Drawing.SystemColors.Control;
            this.insert.Cursor = System.Windows.Forms.Cursors.Hand;
            this.insert.Image = ((System.Drawing.Image)(resources.GetObject("insert.Image")));
            this.insert.Location = new System.Drawing.Point(3, 3);
            this.insert.Name = "insert";
            this.insert.Size = new System.Drawing.Size(34, 34);
            this.insert.TabIndex = 0;
            this.insert.TabStop = false;
            this.insert.Click += new System.EventHandler(this.insert_Click);
            // 
            // edit
            // 
            this.edit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.edit.Image = ((System.Drawing.Image)(resources.GetObject("edit.Image")));
            this.edit.Location = new System.Drawing.Point(3, 41);
            this.edit.Name = "edit";
            this.edit.Size = new System.Drawing.Size(34, 34);
            this.edit.TabIndex = 1;
            this.edit.TabStop = false;
            this.edit.Click += new System.EventHandler(this.edit_Click);
            // 
            // move
            // 
            this.move.Cursor = System.Windows.Forms.Cursors.Hand;
            this.move.Image = ((System.Drawing.Image)(resources.GetObject("move.Image")));
            this.move.Location = new System.Drawing.Point(3, 79);
            this.move.Name = "move";
            this.move.Size = new System.Drawing.Size(34, 34);
            this.move.TabIndex = 2;
            this.move.TabStop = false;
            this.move.Click += new System.EventHandler(this.move_Click);
            // 
            // SideBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.move);
            this.Controls.Add(this.edit);
            this.Controls.Add(this.insert);
            this.Name = "SideBar";
            this.Size = new System.Drawing.Size(39, 264);
            ((System.ComponentModel.ISupportInitialize)(this.insert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.move)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox insert;
        private System.Windows.Forms.PictureBox edit;
        private System.Windows.Forms.PictureBox move;
    }
}
