﻿#region File Description
//-----------------------------------------------------------------------------
// SpriteFontControl.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using System.Collections.Generic;
using NngLevelEditor.Entities;
using NngLevelEditor.Utility;
using System.Windows.Forms;
using WinFormsGraphicsDevice;
using NngLevelEditor.Manager;
using System;
#endregion

namespace NngLevelEditor.Controls
{
    /// <summary>
    /// Example control inherits from GraphicsDeviceControl, which allows it to
    /// render using a GraphicsDevice. This control shows how to use ContentManager
    /// inside a WinForms application. It loads a SpriteFont object through the
    /// ContentManager, then uses a SpriteBatch to draw text. The control is not
    /// animated, so it only redraws itself in response to WinForms paint messages.
    /// </summary>
    public class Canvas : GraphicsDeviceControl
    {
        /// <summary>
        /// Initializes the control, creating the ContentManager
        /// and using it to load a SpriteFont.
        /// </summary>
        protected override void Initialize()
        {
            ContentManager content = new ContentManager(Services, "Content");
            Content.AddContentManager("Content", content);

            GraphicsManager.Initialize(GraphicsDevice);

            SelectionManager.Level = new Level();

            //_track.LoadContent(_content);

            Application.Idle += delegate { Invalidate(); };
        }


        /// <summary>
        /// Disposes the control, unloading the ContentManager.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Content.GetContentManager("Content").Unload();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Draws the control, using SpriteBatch and SpriteFont.
        /// </summary>
        protected override void Draw()
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            GraphicsManager.Draw();
        }

        public void PlaceObject(float x, float y)
        {
            try
            {
                Entity t = SelectionManager.SelectedTool.getEntity();
                Vector2 mouse = new Vector2(x, y);
                Vector2 xnaU = mouse.ToCameraUnits();
                t.Position = xnaU.ToWorldUnits();

                if (SelectionManager.SelectedTool.ToString().Equals("TrackNode"))
                {
                    SelectionManager.Level.AddTrackNode(t);
                }
                else
                {
                    SelectionManager.Level.AddEntity(t);
                }
            }
            catch
            {
                MessageBox.Show("Please select a valid tool.");
            }
        }

        public void SelectObject(float x, float y)
        {
            try
            {
                Vector2 clickPosition = new Vector2(x, y);
                clickPosition = clickPosition.ToCameraUnits();
                clickPosition = clickPosition.ToWorldUnits();
                SelectionManager.Level.SelectEntity(clickPosition);
            }
            catch
            {
                SelectionManager.SelectedEntity = null;
            }
        }

        public void RemoveObject(float x, float y)
        {
            try
            {
                Vector2 clickPosition = new Vector2(x, y);
                clickPosition = clickPosition.ToCameraUnits();
                clickPosition = clickPosition.ToWorldUnits();
                Vector2? selectPosition = SelectionManager.Level.SelectEntity(clickPosition);

                if (selectPosition != null)
                {
                    TrackNode t = new TrackNode();
                    if (SelectionManager.SelectedEntity.GetType().Equals(t.GetType()))
                    {
                        SelectionManager.SelectedEntity.Deselect();
                        SelectionManager.Level.RemoveTrackNode(SelectionManager.SelectedEntity);
                    }
                    else
                    {
                        SelectionManager.SelectedEntity.Deselect();
                        SelectionManager.Level.RemoveEntity(SelectionManager.SelectedEntity);
                    }
                    SelectionManager.SelectedEntity = null;
                }
                else
                {
                    SelectionManager.SelectedEntity = null;
                }
            }
            catch
            {
                SelectionManager.SelectedEntity = null;
            }
        }

        public void MoveObject(float x, float y)
        {
            try
            {
                Vector2 clickPosition = new Vector2(x, y);
                clickPosition = clickPosition.ToCameraUnits();
                clickPosition = clickPosition.ToWorldUnits();

                SelectionManager.SelectedEntity.Position = clickPosition;
            }
            catch
            {

            }
        }

        public void MoveOtherObject(float x, float y)
        {
            try
            {
                Vector2 clickPosition = new Vector2(x, y);
                clickPosition = clickPosition.ToCameraUnits();
                clickPosition = clickPosition.ToWorldUnits();

                if (SelectionManager.SelectedEntity.GetType().Equals((new NngLevelEditor.Entities.Button()).GetType()))
                {
                    ((NngLevelEditor.Entities.Button)SelectionManager.SelectedEntity).EndPosition = clickPosition;
                }
                else if (SelectionManager.SelectedEntity.GetType().Equals((new Necklace()).GetType()))
                {
                    ((Necklace)SelectionManager.SelectedEntity).AltPosition = clickPosition;

                }
            }
            catch
            {

            }
        }
    }
}
