﻿namespace NngLevelEditor.Controls
{
    partial class ToolBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.toolControl5 = new NngLevelEditor.Controls.ToolControl();
            this.toolControl4 = new NngLevelEditor.Controls.ToolControl();
            this.toolControl3 = new NngLevelEditor.Controls.ToolControl();
            this.toolControl2 = new NngLevelEditor.Controls.ToolControl();
            this.toolControl1 = new NngLevelEditor.Controls.ToolControl();
            this.toolControl26 = new NngLevelEditor.Controls.ToolControl();
            this.button1 = new NngLevelEditor.Controls.ToolControl();
            this.node = new NngLevelEditor.Controls.ToolControl();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.toolControl5);
            this.groupBox1.Controls.Add(this.toolControl4);
            this.groupBox1.Controls.Add(this.toolControl3);
            this.groupBox1.Controls.Add(this.toolControl2);
            this.groupBox1.Controls.Add(this.toolControl1);
            this.groupBox1.Controls.Add(this.toolControl26);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.node);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 651);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tools";
            // 
            // toolControl5
            // 
            this.toolControl5.Image = global::NngLevelEditor.Properties.Resources.v1;
            this.toolControl5.Location = new System.Drawing.Point(76, 159);
            this.toolControl5.Name = "toolControl5";
            this.toolControl5.Number = 1;
            this.toolControl5.Size = new System.Drawing.Size(64, 64);
            this.toolControl5.TabIndex = 59;
            this.toolControl5.ToolName = "Velcro";
            // 
            // toolControl4
            // 
            this.toolControl4.Image = global::NngLevelEditor.Properties.Resources.n1;
            this.toolControl4.Location = new System.Drawing.Point(6, 159);
            this.toolControl4.Name = "toolControl4";
            this.toolControl4.Number = 1;
            this.toolControl4.Size = new System.Drawing.Size(64, 64);
            this.toolControl4.TabIndex = 58;
            this.toolControl4.ToolName = "Necklace";
            // 
            // toolControl3
            // 
            this.toolControl3.Image = global::NngLevelEditor.Properties.Resources.k1;
            this.toolControl3.Location = new System.Drawing.Point(146, 89);
            this.toolControl3.Name = "toolControl3";
            this.toolControl3.Number = 1;
            this.toolControl3.Size = new System.Drawing.Size(64, 64);
            this.toolControl3.TabIndex = 57;
            this.toolControl3.ToolName = "Buckle";
            // 
            // toolControl2
            // 
            this.toolControl2.Image = global::NngLevelEditor.Properties.Resources.b4;
            this.toolControl2.Location = new System.Drawing.Point(76, 89);
            this.toolControl2.Name = "toolControl2";
            this.toolControl2.Number = 4;
            this.toolControl2.Size = new System.Drawing.Size(64, 64);
            this.toolControl2.TabIndex = 56;
            this.toolControl2.ToolName = "Button";
            // 
            // toolControl1
            // 
            this.toolControl1.Image = global::NngLevelEditor.Properties.Resources.b3;
            this.toolControl1.Location = new System.Drawing.Point(6, 89);
            this.toolControl1.Name = "toolControl1";
            this.toolControl1.Number = 3;
            this.toolControl1.Size = new System.Drawing.Size(64, 64);
            this.toolControl1.TabIndex = 55;
            this.toolControl1.ToolName = "Button";
            // 
            // toolControl26
            // 
            this.toolControl26.Image = global::NngLevelEditor.Properties.Resources.b2;
            this.toolControl26.Location = new System.Drawing.Point(146, 19);
            this.toolControl26.Name = "toolControl26";
            this.toolControl26.Number = 2;
            this.toolControl26.Size = new System.Drawing.Size(64, 64);
            this.toolControl26.TabIndex = 54;
            this.toolControl26.ToolName = "Button";
            // 
            // button1
            // 
            this.button1.Image = global::NngLevelEditor.Properties.Resources.b1;
            this.button1.Location = new System.Drawing.Point(76, 19);
            this.button1.Name = "button1";
            this.button1.Number = 1;
            this.button1.Size = new System.Drawing.Size(64, 64);
            this.button1.TabIndex = 53;
            this.button1.ToolName = "Button";
            // 
            // node
            // 
            this.node.Image = global::NngLevelEditor.Properties.Resources.o1;
            this.node.Location = new System.Drawing.Point(6, 19);
            this.node.Name = "node";
            this.node.Number = 1;
            this.node.Size = new System.Drawing.Size(64, 64);
            this.node.TabIndex = 26;
            this.node.ToolName = "TrackNode";
            // 
            // ToolBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "ToolBox";
            this.Size = new System.Drawing.Size(219, 651);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ToolControl node;
        private ToolControl button1;
        private ToolControl toolControl26;
        private System.Windows.Forms.GroupBox groupBox1;
        private ToolControl toolControl3;
        private ToolControl toolControl2;
        private ToolControl toolControl1;
        private ToolControl toolControl4;
        private ToolControl toolControl5;

    }
}
