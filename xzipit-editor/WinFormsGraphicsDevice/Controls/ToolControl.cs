﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NngLevelEditor.Manager;
using NngLevelEditor.Tools;

namespace NngLevelEditor.Controls
{
    public partial class ToolControl : UserControl
    {
        public Image Image
        {
            get { return picTool.Image; }
            set { picTool.Image = value; }
        }

        public String ToolName
        {
            get;
            set;
        }

        public int Number
        {
            get;
            set;
        }

        public ToolControl()
        {
            InitializeComponent();
        }

        private void ToolControl_Load(object sender, EventArgs e)
        {

        }

        private void picTool_Click(object sender, EventArgs e)
        {
            SelectionManager.SelectedTool = new Tool(ToolName, Number);
        }
    }
}
