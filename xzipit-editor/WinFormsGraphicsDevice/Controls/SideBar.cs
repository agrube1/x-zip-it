﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NngLevelEditor.Manager;

namespace NngLevelEditor.Controls
{
    public partial class SideBar : UserControl
    {
        public SideBar()
        {
            InitializeComponent();
            insert_Click(null, null);
        }

        private void insert_Click(object sender, EventArgs e)
        {
            Editor.State = Editor.EditorState.Insert;

            SelectButton("insert");
        }

        private void edit_Click(object sender, EventArgs e)
        {
            Editor.State = Editor.EditorState.Edit;
            
            SelectButton("edit");
        }

        private void move_Click(object sender, EventArgs e)
        {
            Editor.State = Editor.EditorState.Move;

            SelectButton("move");
        }

        private void SelectButton(string type)
        {
            insert.BackColor = Color.Lavender;
            edit.BackColor = Color.Lavender;
            move.BackColor = Color.Lavender;

            if (type.Equals("insert"))
            {
                insert.BackColor = Color.DarkBlue;
            }
            if (type.Equals("edit"))
            {
                edit.BackColor = Color.DarkBlue;
            }
            if (type.Equals("move"))
            {
                move.BackColor = Color.DarkBlue;
            }
        }
    }
}
