﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NngLevelEditor.Entities;

namespace NngLevelEditor.Controls
{
    public partial class Properties : UserControl
    {
        public Properties()
        {
            InitializeComponent();
        }

        public void SetSelectedItem(Entity e)
        {
            gridProperties.SelectedObject = e;
        }

        public void ClearSelectedItem()
        {
            gridProperties.SelectedObject = null;
        }
    }
}
