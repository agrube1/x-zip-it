﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NngLevelEditor.Entities;
using NngLevelEditor.Utility;
using NngLevelEditor.Manager;

namespace NngLevelEditor.Controls
{
    public partial class LevelProperties : UserControl
    {
        public LevelProperties()
        {
            InitializeComponent();
        }

        public void SetSelectedItem(Level l)
        {
            gridProperties.SelectedObject = l;
        }

        public void ClearSelectedItem()
        {
            gridProperties.SelectedObject = null;
        }

        private void gridProperties_Click(object sender, EventArgs e)
        {
            SetSelectedItem(SelectionManager.Level);
        }
    }
}
