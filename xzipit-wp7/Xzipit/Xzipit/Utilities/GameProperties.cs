using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FlatRedBall.Input;
using FlatRedBall.Graphics;
using FlatRedBall.Graphics.Animation;
using Microsoft.Devices;
using System.IO;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework.Media;
using FlatRedBall;
using System.Globalization;

namespace Xzipit.Utilities
{
    public static class GameProperties
    {
        public static BitmapFont CustomFont;

        #region Constants
        public const int TOTAL_LEVELS = 20;
        public const int LEVELS_PER_WORLD = 12;
        public const int TOTAL_WORLDS = 4;
        public const float ALPHA_RATE = 5.0f;
        public const float TIME_SCALE = 0.7f;
        #endregion

        #region Properties

        public static bool Vibration
        {
            get;
            set;
        }

        public static bool Sound
        {
            get;
            set;
        }

        public static Song BgMusic
        {
            get;
            set;
        }

        public static bool ExitGame
        {
            set;
            get;
        }

        public static int SelectedLevel
        {
            set;
            get;
        }

        public static int SelectedWorld
        {
            set;
            get;
        }

        public static int LevelPin
        {
            set;
            get;
        }

        public static SaveFile Save
        {
            get;
            private set;
        }

        #endregion

        public static void Initialize()
        {
            ExitGame = false;
            CustomFont = new BitmapFont(@"Content/Fonts/xzipit", @"Content/Fonts/xzipit.fnt", "Global");
            LevelPin = 4;
            BgMusic = FlatRedBallServices.Load<Song>(@"Content/Sound/theme", "Global");
            Microsoft.Xna.Framework.Media.MediaPlayer.IsRepeating = true;
            SelectedWorld = 1;

            Save = new SaveFile();

            LoadSettings();
            Save.LoadTotalPoints();
        }

        public static AnimationChainList GetPins()
        {
            AnimationChainList pin = new AnimationChainList(4);
            AnimationChain pins = new AnimationChain();
            pins.Add(new AnimationFrame(@"Content/MenuObjects/Rewards/gold", 1.0f, "Global"));
            pins.Add(new AnimationFrame(@"Content/MenuObjects/Rewards/silver", 1.0f, "Global"));
            pins.Add(new AnimationFrame(@"Content/MenuObjects/Rewards/bronze", 1.0f, "Global"));
            pins.Add(new AnimationFrame(@"Content/MenuObjects/Rewards/iron", 1.0f, "Global"));

            pin.Add(pins);

            return pin;
        }

        public static void Vibrate(double value)
        {
            if (Vibration)
            {
                VibrateController.Default.Start(TimeSpan.FromSeconds(value));
            }
        }

        #region Settings

        public static void CreateSettings()
        {

            IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();

            //create new file
            using (StreamWriter writeFile = new StreamWriter(new IsolatedStorageFileStream("settings.txt",
                    FileMode.Create, FileAccess.Write, myIsolatedStorage)))
            {
                writeFile.WriteLine("v true");
                writeFile.WriteLine("s true");
                writeFile.WriteLine("p 0");
                writeFile.Flush();
                writeFile.Close();
            }

            myIsolatedStorage.Dispose();
        }

        /// <summary>
        /// Reads the save data record of all levels and returns the raw text.
        /// </summary>
        /// <returns>The raw text from the save file.</returns>
        public static string ReadSettings()
        {
            string text;

            IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            if (!myIsolatedStorage.FileExists("settings.txt"))
            {
                CreateSettings();
            }
            IsolatedStorageFileStream fileStream = myIsolatedStorage.OpenFile("settings.txt", FileMode.Open, FileAccess.Read);
            using (StreamReader reader = new StreamReader(fileStream))
            {
                text = reader.ReadToEnd();
                reader.Close();
            }

            myIsolatedStorage.Dispose();

            return text;
        }

        public static void SaveSettings()
        {
            string writeString = "";
            string[] split;
            string finalString = "";

            writeString = ReadSettings();
            split = writeString.Split('\n');

            foreach (string s in split)
            {
                if (!s.Equals(""))
                {
                    string temp = s.Trim();
                    string[] arr = temp.Split(' ');

                    if (arr.Length == 2)
                    {
                        if (arr[0].Equals("v"))
                        {
                            temp = "v " + GameProperties.Vibration;
                        }
                        else if (arr[0].Equals("s"))
                        {
                            temp = "s " + GameProperties.Sound;
                        }
                    }

                    finalString += temp + "\n";
                }
            }

            IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();

            //create new file
            using (StreamWriter writeFile = new StreamWriter(new IsolatedStorageFileStream("settings.txt",
                    FileMode.Create, FileAccess.Write, myIsolatedStorage)))
            {
                writeFile.Write(finalString);
                writeFile.Flush();
                writeFile.Close();
            }

            myIsolatedStorage.Dispose();
        }

        /// <summary>
        /// Gets the level description, the title and the pin of an individual level.
        /// </summary>
        public static void LoadSettings()
        {
            string text = ReadSettings();
            string[] read = text.Split('\n');


            for (int i = 0; i < read.Length; i++)
            {
                string line = read[i];
                string[] split = line.Split(' ');
                if (split[0].Equals("v"))
                {
                    GameProperties.Vibration = bool.Parse(split[1]);
                }
                else if (split[0].Equals("s"))
                {
                    GameProperties.Sound = bool.Parse(split[1]);
                }
            }
        }

        #endregion

    }
}
