using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FlatRedBall;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using FlatRedBall.Math;
using Xzipit.Utilities.Gestures;
using Xzipit.Utilities.Touch;

namespace Xzipit.Utilities
{
    public static class ExtensionMethods
    {
        public static void PixelScale(this Sprite spr)
        {
            float pixelsPerUnit = SpriteManager.Camera.PixelsPerUnitAt(spr.Z);
            spr.ScaleX = .5f * spr.Texture.Width / pixelsPerUnit;
            spr.ScaleY = .5f * spr.Texture.Height / pixelsPerUnit;
        }

        public static float PixelPerUnit(this Sprite spr)
        {
            return SpriteManager.Camera.PixelsPerUnitAt(spr.Z);
        }

        public static float PixelScaleX(this Sprite spr)
        {
            return .5f * spr.Texture.Width / SpriteManager.Camera.PixelsPerUnitAt(spr.Z);
        }

        public static float PixelScaleY(this Sprite spr)
        {
            return .5f * spr.Texture.Height / SpriteManager.Camera.PixelsPerUnitAt(spr.Z);
        }

        public static void RotateTowards(this Sprite spr, Vector3 pt, bool attached)
        {
            float faceAtX = pt.X;
            float faceAtY = pt.Y;

            float rotation = (float)Math.Atan2(faceAtY, faceAtX) - (float)Math.PI / 2.0f;

            if (attached)
                spr.RelativeRotationZ = rotation;
            else
                spr.RotationZ = rotation;
        }

        public static String ToTimeString(this double time)
        {
            int MAX_TIME = 6000;

            if (time < MAX_TIME)
            {
                int seconds, minutes, milliseconds;
                seconds = ((int)time) % 60;
                minutes = ((int)time) / 60;
                milliseconds = (int)((time - (int)time) * 1000);
                return String.Format("{0:0#}:{1:0#}.{2:0##}", minutes, seconds, milliseconds);
            }
            else
                return String.Format("{0:0#}:{1:0#}.{2:0##}", 99, 59, 999);
        }

        public static void PlaySound(this SoundEffect s)
        {
            if (GameProperties.Sound)
            {
                s.Play();
            }
        }

        public static Vector3 PixelToWorld(this Vector2 pixCoord, float z)
        {
            float x = 0.0f, y = 0.0f;

            MathFunctions.WindowToAbsolute(
                (int)pixCoord.X,
                (int)pixCoord.Y,
                ref x,
                ref y,
                z,
                SpriteManager.Camera,
                Camera.CoordinateRelativity.RelativeToWorld);

            return new Vector3(x, y, z);
        }

        public static GameGestureType ToGameGestureType(this SimpleGestureType simpleGestureType)
        {
            switch (simpleGestureType)
            {
                case SimpleGestureType.FreeDrag:
                case SimpleGestureType.HorizontalDrag:
                case SimpleGestureType.VerticalDrag:
                    return GameGestureType.Drag;
                case SimpleGestureType.DragComplete:
                    return GameGestureType.Pull;
                case SimpleGestureType.Tap:
                case SimpleGestureType.DoubleTap:
                    return GameGestureType.Tap;
                case SimpleGestureType.Hold:
                    return GameGestureType.Hold;
                case SimpleGestureType.None:
                default:
                    return GameGestureType.None;
            }
        }

        public static bool HasFlag(this SimpleGestureType e, SimpleGestureType flag)
        {
            return (e & flag) == flag;
        }

        public static void Move(this Screens.Screen s, String screen)
        {
            s.MoveToScreen(screen);
            GameGestureManager.LastScreenMoveTime = TimeManager.CurrentTime;
        }
    }
}
