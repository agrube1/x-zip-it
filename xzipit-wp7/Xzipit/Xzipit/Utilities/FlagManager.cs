using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FlatRedBall.Input;
using FlatRedBall.Graphics;
using FlatRedBall.Graphics.Animation;

namespace Xzipit.Utilities
{
    public static class FlagManager
    {
        private static bool _exitGame;
        private static bool _winLevel;
        private static bool _doneLevel;
        private static double _winTime;
        private static int _selectedLevel;
        public static BitmapFont CustomFont;
        public static int NumLevels;

        private const int MAX_TIME = 6000;

        public static bool NextLevel
        {
            get;
            set;
        }

        public static bool ExitGame
        {
            set { _exitGame = value; }
            get { return _exitGame; }
        }

        public static bool WinLevel
        {
            set { _winLevel = value; }
            get { return _winLevel; }
        }

        public static bool DoneLevel
        {
            set { _doneLevel = value; }
            get { return _doneLevel; }
        }

        public static double WinTime
        {
            set { _winTime = value; }
            get { return _winTime; }
        }

        public static int SelectedLevel
        {
            set { _selectedLevel = value; }
            get { return _selectedLevel; }
        }

        public static void Initialize()
        {
            _exitGame = false;
            CustomFont = new BitmapFont(@"Content\Fonts\selection", @"Content\Fonts\selection.fnt", "Global");
            _winLevel = false;
            _doneLevel = false;
            NextLevel = false;
            NumLevels = 20;
        }

        public static AnimationChainList GetPins()
        {
            AnimationChainList pin = new AnimationChainList(4);
            AnimationChain pins = new AnimationChain();
            pins.Add(new AnimationFrame(@"Content/MenuObjects/Rewards/gold", 1.0f, "Global"));
            pins.Add(new AnimationFrame(@"Content/MenuObjects/Rewards/silver", 1.0f, "Global"));
            pins.Add(new AnimationFrame(@"Content/MenuObjects/Rewards/bronze", 1.0f, "Global"));
            pins.Add(new AnimationFrame(@"Content/MenuObjects/Rewards/iron", 1.0f, "Global"));

            pin.Add(pins);

            return pin;
        }

        public static String ConvertSecondsToTime(double time)
        {
            if (time < MAX_TIME)
            {
                int seconds, minutes, milliseconds;
                seconds = ((int)time) % 60;
                minutes = ((int)time) / 60;
                milliseconds = (int)((time - (int)time) * 1000);
                return String.Format("{0:0#}:{1:0#}.{2:0##}", minutes, seconds, milliseconds);
            } else
                return String.Format("{0:0#}:{1:0#}.{2:0##}", 99, 59, 999);
        }
    }
}
