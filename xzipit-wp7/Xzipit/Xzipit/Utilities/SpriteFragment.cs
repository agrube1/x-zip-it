using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using System.Windows.Shapes;
using FlatRedBall;
using Microsoft.Xna.Framework;

namespace Xzipit.Utilities
{
    public class SpriteFragment
    {
        private Texture2D _leftTexture;
        private Texture2D _rightTexture;
        private Sprite _leftSprite;
        private Sprite _rightSprite;

        private int _bg;

        private const float X_EDGE = 10.0f;
        private const float Y_EDGE = 16.0f;

        private const float X_VEL = 25.0f;

        public SpriteFragment(int bg)
        {
            _bg = bg;
            CreateTextures();
        }

        //private void CreateBounds()
        //{
        //    Vector3 start = _bounds[0];
        //    Vector3 end = _bounds[_bounds.Count - 1];

        //    Vector3 finalStart = Vector3.Zero;
        //    Vector3 finalEnd = Vector3.Zero;

        //    if (start.X < 0.0f)
        //    {
        //        if (start.Y < 0.0f)
        //        {
        //            finalStart = new Vector3(-X_EDGE, -Y_EDGE, 0.0f);
        //        }
        //        else if (start.Y >= 0.0f)
        //        {
        //            finalStart = new Vector3(-X_EDGE, Y_EDGE, 0.0f);
        //        }
        //    }
        //    else if (start.X >= 0.0f)
        //    {
        //        if (start.Y < 0.0f)
        //        {
        //            finalStart = new Vector3(X_EDGE, -Y_EDGE, 0.0f);
        //        }
        //        else if (start.Y >= 0.0f)
        //        {
        //            finalStart = new Vector3(X_EDGE, Y_EDGE, 0.0f);
        //        }
        //    }

        //    if (end.X < 0.0f)
        //    {
        //        if (end.Y < 0.0f)
        //        {
        //            if (finalStart.X != -X_EDGE && finalStart.Y != -Y_EDGE)
        //            {
        //                finalEnd = new Vector3(-X_EDGE, -Y_EDGE, 0.0f);
        //            }
        //            else
        //            {
        //                finalEnd = new Vector3(-X_EDGE, Y_EDGE, 0.0f);
        //            }
        //        }
        //        else if (end.Y >= 0.0f)
        //        {
        //            if (finalStart.X != -X_EDGE && finalStart.Y != Y_EDGE)
        //            {
        //                finalEnd = new Vector3(-X_EDGE, Y_EDGE, 0.0f);
        //            }
        //            else
        //            {
        //                finalEnd = new Vector3(-X_EDGE, -Y_EDGE, 0.0f);
        //            }
        //        }
        //    }
        //    else if (end.X >= 0.0f)
        //    {
        //        if (end.Y < 0.0f)
        //        {
        //            if (finalStart.X != X_EDGE && finalStart.Y != -Y_EDGE)
        //            {
        //                finalEnd = new Vector3(X_EDGE, -Y_EDGE, 0.0f);
        //            }
        //            else
        //            {
        //                finalEnd = new Vector3(X_EDGE, Y_EDGE, 0.0f);
        //            }
        //        }
        //        else if (end.Y >= 0.0f)
        //        {
        //            if (finalStart.X != -X_EDGE && finalStart.Y != Y_EDGE)
        //            {
        //                finalEnd = new Vector3(X_EDGE, Y_EDGE, 0.0f);
        //            }
        //            else
        //            {
        //                finalEnd = new Vector3(X_EDGE, -Y_EDGE, 0.0f);
        //            }
        //        }
        //    }

        //    Vector3 missingCorner = Vector3.Zero;

        //    if (finalStart.X != finalEnd.X
        //        && finalStart.Y != finalEnd.Y)
        //    {

        //    }
        //}

        private void CreateTextures()
        {
            Texture2D texture = FlatRedBallServices.Load<Texture2D>(@"Content\Backgrounds\bg" + 1);
            Color[] leftPixels = new Color[texture.Width * texture.Height];
            Color[] rightPixels = new Color[texture.Width * texture.Height];
            texture.GetData<Color>(leftPixels);
            texture.GetData<Color>(rightPixels);

            int curHeight = 0;

            for (int i = 0; i < texture.Width * texture.Height; i++)
            {
                if (i / texture.Width > curHeight)
                    curHeight++;

                if (i - (texture.Width * curHeight) < texture.Width / 2)
                {
                    rightPixels[i] = new Color(0.0f, 0.0f, 0.0f, 0.0f);
                }
                else
                {
                    leftPixels[i] = new Color(0.0f, 0.0f, 0.0f, 0.0f);
                }
            }

            _leftTexture = new Texture2D(texture.GraphicsDevice,
                                                   texture.Width, texture.Height);
            _rightTexture = new Texture2D(texture.GraphicsDevice,
                                                   texture.Width, texture.Height);
            _leftTexture.SetData(leftPixels);
            _rightTexture.SetData(rightPixels);
            _leftSprite = SpriteManager.AddSprite(_leftTexture);
            _rightSprite = SpriteManager.AddSprite(_rightTexture);
        }

        public void FadeOut()
        {
            _leftSprite.XVelocity = -X_VEL;
            _rightSprite.XVelocity = X_VEL;
        }

        public bool DoneFading()
        {
            if (_leftSprite.X < -40.0f)
            {
                return true;
            }

            return false;
        }
    }
}
