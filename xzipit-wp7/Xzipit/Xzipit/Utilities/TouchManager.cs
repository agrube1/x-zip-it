using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FlatRedBall;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Input;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;

namespace Xzipit.Utilities
{
    public static class TouchManager
    {
        private static bool mFirstTimeInitialized = true;

        private static Vector3 mStartDown;
        private static Vector3 mEndUp;
        private static Vector3 mCurrentDown;

        public enum Gesture
        {
            Drag,
            Pull,
            Tap,
            Pinch,
            PinchCompleted,
            None
        }

        private static Gesture mCurrentGesture;

        private static float TAP_TOLERANCE = 1.5f;

        public static Vector3 StartPostion
        {
            get 
            {
                if (mCurrentGesture == Gesture.None)
                    System.Diagnostics.Debug.WriteLine("Warning: Using start touch position, but user has not touched screen.");

                return mStartDown; 
            }
        }

        public static Vector3 EndPosition
        {
            get 
            {
                if (mCurrentGesture == Gesture.None || mCurrentGesture == Gesture.Drag)
                    System.Diagnostics.Debug.WriteLine("Warning: Using end touch position, but user has not touched screen or is still touching the screen.");

                return mEndUp; 
            }
        }

        public static Vector3 CurrentPosition
        {
            get 
            { 
                if (mCurrentGesture == Gesture.None || mCurrentGesture == Gesture.Pull || mCurrentGesture == Gesture.Tap)
                    System.Diagnostics.Debug.WriteLine("Warning: Using current touch position, but user has not touched screen or finished touching the screen.");

                return mCurrentDown; 
            }
        }

        public static Gesture CurrentGesture
        {
            get { return mCurrentGesture; }
        }

        public static void Initialize()
        {
            if (mFirstTimeInitialized)
            {
                mStartDown = new Vector3();
                mEndUp = new Vector3();
                mCurrentDown = new Vector3();

                mFirstTimeInitialized = false;
            }

            mCurrentGesture = Gesture.None;
        }

        public static void Update()
        {
            TouchPanel.EnabledGestures = GestureType.Pinch | GestureType.PinchComplete | GestureType.Flick;
            //TouchPanel.
            if (InputManager.TouchScreen.PinchStarted)
            {
                mStartDown.X = InputManager.TouchScreen.WorldXAt(0.0f);
                mStartDown.Y = InputManager.TouchScreen.WorldYAt(0.0f);
                mCurrentGesture = Gesture.Pinch;
            }
            else if (InputManager.TouchScreen.PinchStopped)
            {
                mEndUp.X = InputManager.TouchScreen.WorldXAt(0.0f);
                mEndUp.Y = InputManager.TouchScreen.WorldYAt(0.0f);
                mCurrentGesture = Gesture.PinchCompleted;
            }
            else if (InputManager.TouchScreen.ScreenPushed)
            {
                mStartDown.X = InputManager.TouchScreen.WorldXAt(0.0f);
                mStartDown.Y = InputManager.TouchScreen.WorldYAt(0.0f);

                //mCurrentGesture = Gesture.Drag;
            }
            else if (InputManager.TouchScreen.ScreenReleased)
            {
                mEndUp.X = InputManager.TouchScreen.WorldXAt(0.0f);
                mEndUp.Y = InputManager.TouchScreen.WorldYAt(0.0f);

                if (Vector3.Distance(mStartDown, mEndUp) > TAP_TOLERANCE)
                    mCurrentGesture = Gesture.Pull;
                else
                    mCurrentGesture = Gesture.Tap;
            }
            else if (InputManager.TouchScreen.ScreenDown)
            {
                mCurrentDown.X = InputManager.TouchScreen.WorldXAt(0.0f);
                mCurrentDown.Y = InputManager.TouchScreen.WorldYAt(0.0f);

                if (Vector3.Distance(mStartDown, mCurrentDown) > TAP_TOLERANCE)
                    mCurrentGesture = Gesture.Drag;
                else if (mCurrentGesture != Gesture.Drag)
                    mCurrentGesture = Gesture.None;
            }
            else
            {
                mCurrentGesture = Gesture.None;
            }
        }
    }
}
