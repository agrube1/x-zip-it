using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using FlatRedBall.Graphics;
using FlatRedBall;
using Microsoft.Xna.Framework;
using FlatRedBall.Math.Geometry;

namespace Xzipit.Utilities
{
    public class Background : PositionedObject
    {
        #region Fragment
        public class SpriteFragment
        {
            private Texture2D _leftTexture;
            private Texture2D _rightTexture;
            private Sprite _leftSprite;
            private Sprite _rightSprite;

            private int _bg;

            private const float X_EDGE = 10.0f;
            private const float Y_EDGE = 16.0f;

            private const float X_VEL = 25.0f;

            public SpriteFragment(int bg)
            {
                _bg = bg;
                CreateTextures();
            }

            private void CreateTextures()
            {
                Texture2D texture = FlatRedBallServices.Load<Texture2D>(@"Content\Backgrounds\bg" + 1);
                Color[] leftPixels = new Color[texture.Width * texture.Height];
                Color[] rightPixels = new Color[texture.Width * texture.Height];
                texture.GetData<Color>(leftPixels);
                texture.GetData<Color>(rightPixels);

                int curHeight = 0;

                for (int i = 0; i < texture.Width * texture.Height; i++)
                {
                    if (i / texture.Width > curHeight)
                        curHeight++;

                    if (i - (texture.Width * curHeight) < texture.Width / 2)
                    {
                        rightPixels[i] = new Color(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                    else
                    {
                        leftPixels[i] = new Color(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }

                _leftTexture = new Texture2D(texture.GraphicsDevice,
                                                       texture.Width, texture.Height);
                _rightTexture = new Texture2D(texture.GraphicsDevice,
                                                       texture.Width, texture.Height);
                _leftTexture.SetData(leftPixels);
                _rightTexture.SetData(rightPixels);
                _leftSprite = SpriteManager.AddSprite(_leftTexture);
                _rightSprite = SpriteManager.AddSprite(_rightTexture);

                _leftSprite.PixelScale();
                _leftSprite.Z = -0.1f;
                _rightSprite.PixelScale();
                _rightSprite.Z = -0.1f;
            }

            public void FadeOut()
            {
                _leftSprite.XVelocity = -X_VEL;
                _rightSprite.XVelocity = X_VEL;
            }

            public bool DoneFading()
            {
                if (_leftSprite.X < -40.0f)
                {
                    _leftSprite.XVelocity = 0.0f;
                    _leftSprite.X = 0.0f;
                    _rightSprite.XVelocity = 0.0f;
                    _rightSprite.X = 0.0f;
                    return true;
                }

                return false;
            }

            public void Destroy()
            {
                SpriteManager.RemoveSprite(_leftSprite);
                SpriteManager.RemoveSprite(_rightSprite);
            }
        }

#endregion

        private int _bgNum;
        private Sprite _background;
        private SpriteFragment _split;

        public Background(int bgNum)
        {
            _bgNum = bgNum;

            Initialize(true);
        }

        #region Initialization
        protected virtual void Initialize(bool addToManagers)
        {
            // Here you can preload any content you will be using
            // like .scnx files or texture files.

            if (addToManagers)
            {
                AddToManagers(null);
            }
        }

        public virtual void AddToManagers(Layer layerToAddTo)
        {
            SpriteManager.AddPositionedObject(this);

            _background = SpriteManager.AddSprite(@"Content\Backgrounds\bg" + _bgNum, "Global");
            _background.Z = -0.1f;
            _background.PixelScale();

            _split = new SpriteFragment(_bgNum);

        }

        #endregion

        #region Update

        public virtual void Activity()
        {
           
        }

        #endregion

        #region Destroy

        public virtual void Destroy()
        {
            // Remove self from the SpriteManager:
            SpriteManager.RemovePositionedObject(this);

            SpriteManager.RemoveSprite(_background);

            _split.Destroy();
        }

        public void FadeOut()
        {
            _split.FadeOut();
        }

        public virtual bool DoneFading()
        {
            return _split.DoneFading();
        }

        #endregion
    }
}
