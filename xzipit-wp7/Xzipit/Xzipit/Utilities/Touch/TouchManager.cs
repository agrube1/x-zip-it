using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xzipit.Interfaces;
using Xzipit.Utilities.Gestures;
using Xzipit.GameObjects;

namespace Xzipit.Utilities.Touch
{
    public static class TouchManager
    {
        private static List<ITappable> _tappables = new List<ITappable>();
        private static List<IDraggable> _draggables = new List<IDraggable>();
        private static List<IPullable> _pullables = new List<IPullable>();
        private static List<IPinchable> _pinchables = new List<IPinchable>();

        public static GameGesture ZipperDrag;

        #region Tappables
        public static void AddTappable(ITappable t)
        {
            _tappables.Add(t);
        }

        public static void RemoveTappable(ITappable t)
        {
            try
            {
                _tappables.Remove(t);
            }
            catch
            {
                
            }
        }
        #endregion

        #region Draggables
        public static void AddDraggable(IDraggable t)
        {
            _draggables.Add(t);
        }

        public static void RemoveDraggable(IDraggable t)
        {
            try
            {
                _draggables.Remove(t);
            }
            catch
            {
                
            }
        }

        #endregion

        #region Pullable
        public static void AddPullable(IPullable t)
        {
            _pullables.Add(t);
        }

        public static void RemovePullable(IPullable t)
        {
            try
            {
                _pullables.Remove(t);
            }
            catch
            {

            }
        }

        #endregion

        #region Pinchable
        public static void AddPinchable(IPinchable t)
        {
            _pinchables.Add(t);
        }

        public static void RemovePinchable(IPinchable t)
        {
            try
            {
                _pinchables.Remove(t);
            }
            catch
            {

            }
        }

        #endregion

        public static void Update()
        {
            foreach (GameGesture g in GameGestureManager.Gestures)
            {
                if (g.GameGestureType == GameGestureType.Pinch)
                {
                    foreach (IPinchable p in _pinchables)
                    {
                        if (p.WasPinched(g))
                        {
                            p.OnPinch();
                            break;
                        }
                    }
                }
                else if (g.GameGestureType == GameGestureType.Drag)
                {
                    foreach (IDraggable p in _draggables)
                    {
                        if (p.WasDragged(g))
                        {
                            if (p.OnDrag(g))
                            {
                                break;
                            }
                        }
                    }
                }
                else if (g.GameGestureType == GameGestureType.Pull)
                {
                    foreach (IPullable p in _pullables)
                    {
                        if (p.WasPulled(g))
                        {
                            p.OnPull(g);
                            break;
                        }
                    }
                }
                else if (g.GameGestureType == GameGestureType.Tap)
                {
                    foreach (ITappable p in _tappables)
                    {
                        if (p.WasTapped(g))
                        {
                            p.OnTap();
                            break;
                        }
                    }
                }
            }
        }

        public static void Update(double time)
        {
            foreach (GameGesture g in GameGestureManager.Gestures)
            {
                if (g.GameGestureType == GameGestureType.Pinch)
                {
                    foreach (IPinchable p in _pinchables)
                    {
                        if (p.WasPinched(g))
                        {
                            p.OnPinch(time);
                        }
                    }
                }
                else if (g.GameGestureType == GameGestureType.Drag)
                {
                    foreach (IDraggable p in _draggables)
                    {
                        if (p.WasDragged(g))
                        {
                            p.OnDrag(g);
                            break;
                        }
                    }
                }
                else if (g.GameGestureType == GameGestureType.Pull)
                {
                    foreach (IPullable p in _pullables)
                    {
                        if (p.WasPulled(g))
                        {
                            p.OnPull(g);
                            break;
                        }
                    }
                }
                else if (g.GameGestureType == GameGestureType.Tap)
                {
                    foreach (ITappable p in _tappables)
                    {
                        if (p.WasTapped(g))
                        {
                            p.OnTap();
                            break;
                        }
                    }
                }
            }
        }

        public static void Update(GameGesture zipGesture, double time)
        {
            foreach (GameGesture g in GameGestureManager.Gestures)
            {
                if (g.GameGestureType == GameGestureType.Pinch)
                {
                    foreach (IPinchable p in _pinchables)
                    {
                        if (p.WasPinched(g))
                        {
                            p.OnPinch(time);
                            break;
                        }
                    }
                }
                else if (g.GameGestureType == GameGestureType.Drag)
                {
                    foreach (IDraggable p in _draggables)
                    {
                        if (g.Id == zipGesture.Id)
                        {
                            Zipper z = new Zipper();
                            if (z.GetType().Equals(p.GetType()) && p.WasDragged(g))
                            {
                                p.OnDrag(g);
                            }
                        }
                        else
                        {
                            if (p.WasDragged(g))
                            {
                                if (p.OnDrag(g))
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (g.GameGestureType == GameGestureType.Pull)
                {
                    foreach (IPullable p in _pullables)
                    {
                        if (p.WasPulled(g))
                        {
                            p.OnPull(g);
                            break;
                        }
                    }
                }
                else if (g.GameGestureType == GameGestureType.Tap)
                {
                    foreach (ITappable p in _tappables)
                    {
                        if (p.WasTapped(g))
                        {
                            p.OnTap();
                            break;
                        }
                    }
                }
            }
        }
    }
}
