using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Xzipit.Utilities.Touch
{
    [Flags]
    public enum SimpleGestureType
    {
        None = 0,
        Tap = 1,
        FreeDrag = 2,
        HorizontalDrag = 4,
        VerticalDrag = 8,
        DragComplete = 16,
        Hold = 32,
        DoubleTap = 64
    }

    public struct SimpleGestureSample
    {
        private int _id;
        private Vector2 _position;
        private Vector2 _delta;
        private TimeSpan _timestamp;
        private SimpleGestureType _simpleGestureType;

        public Vector2 Position
        {
            get { return _position; }
        }
        public Vector2 Delta
        {
            get { return _delta; }
        }
        public SimpleGestureType SimpleGestureType
        {
            get { return _simpleGestureType; }
        }
        public TimeSpan Timestamp
        {
            get { return _timestamp; }
        }
        public int Id
        {
            get { return _id; }
        }

        public SimpleGestureSample(int id, SimpleGestureType simpleGestureType, TimeSpan timestamp, Vector2 position, Vector2 delta)
        {
            _simpleGestureType = SimpleGestureType.None;
            _timestamp = new TimeSpan();
            _position = new Vector2();
            _delta = new Vector2();

            _id = id;
            _simpleGestureType = simpleGestureType;
            _timestamp = new TimeSpan(timestamp.Days, timestamp.Hours, timestamp.Minutes, timestamp.Seconds, timestamp.Milliseconds);
            _position = new Vector2(position.X, position.Y);
            _delta = new Vector2(delta.X, delta.Y);
        }
    }
}
