using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using FlatRedBall.Math.Geometry;

namespace Xzipit.Utilities.Gestures
{
    public class GameGesturePair
    {
        private GameGesture _g1;
        private GameGesture _g2;

        private const float PINCH_END_DISTANCE = 3.5f;

        public bool IsPinch
        {
            get { return TestIfPinch(); }
        }

        public Vector3 AveragePosition
        {
            get { return GetAveragePosition(); }
        }

        public GameGesturePair(GameGesture g1, GameGesture g2)
        {
            _g1 = g1;
            _g2 = g2;
        }

        private bool TestIfPinch()
        {
            float startDistance = (_g1.Start - _g2.Start).Length();
            float endDistance = (_g1.End - _g2.End).Length();

            //Debug.WriteLine("Start Dist: " + startDistance + "\nEnd Dist: " + endDistance);
            // the two drags must start a certain distance away from eachother
            // and end a certain distance away from eachother to be considered a pinch
            if (startDistance > endDistance)
                //|| endDistance < PINCH_END_DISTANCE)
            {
                return true;
            }

            return false;
        }

        private Vector3 GetAveragePosition()
        {
            float xavg, yavg;

            xavg = (_g1.End.X + _g2.End.X) / 2;
            yavg = (_g1.End.Y + _g2.End.Y) / 2;

            return new Vector3(xavg, yavg, 0.0f);
        }

        public bool Contains(GameGesture g)
        {
            return (g.Id == _g1.Id || g.Id == _g2.Id);
        }

        public void Discard()
        {

        }
    }
}
