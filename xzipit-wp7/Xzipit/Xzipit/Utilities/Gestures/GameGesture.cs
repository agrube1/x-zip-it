using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Xzipit.Utilities.Gestures
{
    public enum GameGestureType
    {
        None,
        Drag,
        Pull,
        Tap,
        Pinch,
        Hold
    }

    public struct GameGesture
    {
        private GameGestureType _gameGestureType;
        private Vector3 _start;
        private Vector3 _end;
        private double _startTime;
        private int _id;

        public GameGestureType GameGestureType
        {
            get { return _gameGestureType; }
            set { _gameGestureType = value; }
        }
        public Vector3 Start
        {
            get { return _start; }
        }
        public Vector3 End
        {
            get { return _end; }
        }
        public double StartTime
        {
            get { return _startTime; }
        }

        public int Id
        {
            get { return _id; }
        }

        public GameGesture(GameGestureType gameGestureType, Vector3 start, Vector3 end, double startTime, int id)
        {
            _gameGestureType = GameGestureType.None;
            _start = new Vector3();
            _end = new Vector3();

            _gameGestureType = gameGestureType;
            _start = start;
            _end = end;
            _startTime = startTime;
            _id = id;
        }

        public string ToString()
        {
            return "Gesture Type: " + _gameGestureType + " - Start Time: " + _startTime
                + " - Start Pos: " + _start + " - End Pos: " + _end;
        }

        public bool Equals(GameGesture g)
        {
            return (g.Id == this.Id);
        }

        public static Vector3 GetAveragePosition(GameGesture g1, GameGesture g2)
        {
            float xavg, yavg;

            xavg = (g1.End.X + g2.End.X) / 2;
            yavg = (g1.End.Y + g2.End.Y) / 2;

            return new Vector3(xavg, yavg, 0.0f);
        }

    }
}
