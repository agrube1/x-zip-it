using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Xzipit.Utilities.Gestures;

namespace Xzipit.Interfaces
{
    public interface IPullable
    {
        bool WasPulled(GameGesture g);
        void OnPull(GameGesture g);
    }
}
