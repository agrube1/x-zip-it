using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Xzipit.Utilities.Gestures;

namespace Xzipit.Interfaces
{
    public interface IDraggable
    {
        bool WasDragged(GameGesture g);
        bool OnDrag(GameGesture g);
    }
}
