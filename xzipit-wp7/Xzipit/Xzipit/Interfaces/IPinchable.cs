using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xzipit.Utilities.Gestures;

namespace Xzipit.Interfaces
{
    public interface IPinchable
    {
        bool WasPinched(GameGesture g);
        void OnPinch();     
        void OnPinch(double startTime);
    }
}
