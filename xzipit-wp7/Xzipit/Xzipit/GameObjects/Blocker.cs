using System;
using System.Collections.Generic;

using FlatRedBall;
using FlatRedBall.Math.Geometry;

using Microsoft.Xna.Framework;
using Microsoft.Devices;
using Microsoft.Xna.Framework.Audio;

using Xzipit.Utilities;
using Xzipit.Interfaces;
using Xzipit.Utilities.Gestures;
using System.Diagnostics;
using Xzipit.Utilities.Touch;

namespace Xzipit.GameObjects
{
    class Blocker : Obstacle
    {
        #region Fields

        private int _type;
        private Circle _touchCollision;
        
        private const float TEXTURE_HEIGHT = 1.24264073f;

        private SoundEffect _touchSound;

        // Keep the ContentManager for easy access:

        #endregion

        #region Methods

        #region Constructors
        public Blocker(string contentManagerName)
        {
            // Set the default type. This constructor should only be used for testing.
            _contentManagerName = contentManagerName;
            _type = 1;

            Initialize(true);
        }

        public Blocker(string contentManagerName, float x, float y, int type)
        {
            // Sets parameters of a Button based on a level file.
            _contentManagerName = contentManagerName;
            _type = type;
            this.Position.X = x;
            this.Position.Y = y;
            this.Position.Z = 0.1f;

            Initialize(true);
        }

        #endregion

        #region Initialization
        protected override void Initialize(bool addToManagers)
        {
            // Here you can preload any content you will be using
            // like .scnx files or texture files.

            if (addToManagers)
            {
                AddToManagers();
            }
        }

     

        public virtual void AddToManagers()
        {
            SpriteManager.AddPositionedObject(this);

            _sprite = SpriteManager.AddSprite(@"Content\GameObjects\Obstacles\button" + _type, _contentManagerName);

            // create a collision box for button touches. Is bigger than the sprite scale so the
            // player need not be as precise. Also accomodates for big fingers.
            _touchCollision = ShapeManager.AddCircle();
            _touchCollision.Radius = _sprite.ScaleX * 3.0f;
            _touchCollision.AttachTo(this, false);
            _touchCollision.Visible = false;

            // The tooltip that comes on screen if it is a tutorial level.
            SetTooltip("dodge");
            _tooltip.RelativeY = 1.0f;
            _tooltip.RelativeZ = 0.15f;

            // Load the sound effect for tapping the button.
            _touchSound = FlatRedBallServices.Load<SoundEffect>(@"Content\Sound\button");

            base.AddToManagers(0.8f, 0.8f);
        }

        #endregion

        #region Update
        public override void Activity(GameGesture zipGesture, double t)
        {
            // flashes the tutorial tooltip
            if (_tooltip.Visible)
            {
                PlayTooltip();
            }
        }

        public override void Collide(double t)
        {
            return;
        }

        #endregion

        #region Destroy
        public override void Destroy()
        {
            base.Destroy();

            ShapeManager.Remove(_touchCollision);
        }

        #endregion

        #region Private Methods

        #region Tooltip
        // Flashes the tap this indicator.
        protected override void PlayTooltip()
        {
            if (_tooltip.Alpha == 0.0f)
            {
                _tooltip.AlphaRate = 4.5f;
            }
            else if (_tooltip.Alpha == 1.0f)
            {
                _tooltip.AlphaRate = -1.0f;
            }
        }

        #endregion

        // Reverses the move to direction of the button once it reaches it's destination.
        private void ReverseDirection()
        {
            this.Velocity = Vector3.Zero;
            this.Position.Z = 0.1f;
            _sprite.Alpha = 1.0f;
            _enabled = true;
        }

        #endregion

        #endregion

    }
}
