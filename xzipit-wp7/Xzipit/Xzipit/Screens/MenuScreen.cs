using System;
using FlatRedBall;
using FlatRedBall.Input;
using Microsoft.Devices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Xzipit.MenuObjects;
using Xzipit.Utilities;
using FlatRedBall.Math.Geometry;
using Xzipit.Utilities.Gestures;
using System.Diagnostics;
 
namespace Xzipit.Screens
{
    public class MenuScreen : Screen
    {
        private MenuButton _levelSelect;
        private MenuButton _exit;
        private MenuButton _settings;
        private Sprite _logo;
        private Sprite _bg;

        #region Methods

        #region Constructor and Initialize

        public MenuScreen()
            : base("MenuScreen")
        {
            // Don't put initialization code here, do it in
            // the Initialize method below
            //   |   |   |   |   |   |   |   |   |   |   |
            //   |   |   |   |   |   |   |   |   |   |   |
            //   V   V   V   V   V   V   V   V   V   V   V

        }

        public override void Initialize(bool addToManagers)
        {
            // Put menu buttons here.
            // Menu buttons should include level select, directions, credits, and exit. 

            _levelSelect = new MenuButton("Global", "level-select");
            _exit = new MenuButton("Global", "exit");
            _settings = new MenuButton("Global", "settings");

            // AddToManagers should be called LAST in this method:
            if (addToManagers)
            {
                AddToManagers();
            }
        }

        public override void AddToManagers()
        {
            string contentManagerName = "Global";

            _logo = SpriteManager.AddSprite(@"Content\MenuObjects\logo", contentManagerName);
            _logo.X = -0.5f;
            _logo.Y = 10.0f;
            float pixelsPerUnit = SpriteManager.Camera.PixelsPerUnitAt(_logo.Z);
            _logo.ScaleX = .6f * _logo.Texture.Width / pixelsPerUnit;
            _logo.ScaleY = .6f * _logo.Texture.Height / pixelsPerUnit;

            _bg = SpriteManager.AddSprite(@"Content\Backgrounds\menubg", "Global");
            _bg.Z = -0.1f;
            _bg.ScaleX = .5f * _bg.Texture.Width / pixelsPerUnit;
            _bg.ScaleY = .5f * _bg.Texture.Height / pixelsPerUnit;
            _bg.RotationZ = (float)Math.PI / 2;

            _levelSelect.Position.Y = 1.5f;
            _exit.Position.Y = -10.5f;
            _settings.Position.Y = -4.5f;
            
        }

        #endregion

        #region Public Methods

        public override void Activity(bool firstTimeCalled)
        {
            if (!firstTimeCalled)
            {
                // Check for input against buttons here.
                GameGestureManager.Update();

                HandleGestures();

                //GestureManager.Update(0.0f);
                //HandleCollisions();

                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed && !firstTimeCalled)
                {
                    this.Destroy();
                    GameProperties.ExitGame = true;
                }
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            _exit.Destroy();
            _levelSelect.Destroy();
            _settings.Destroy();
            SpriteManager.RemoveSprite(_logo);
            SpriteManager.RemoveSprite(_bg);
        }

        //private void HandleCollisions()
        //{
        //    // level select button collision
        //    if (GestureManager.CurGesture == GestureManager.Gesture.Tap
        //        && _levelSelect.Collision.IsPointInside(ref GestureManager.CurTouchPoint))
        //    {
        //        _levelSelect.PlaySound();
        //        this.MoveToScreen(typeof(Screens.WorldSelectScreen).FullName);
        //        GameProperties.Vibrate(.05);
        //    }
        //        // exit button collision
        //    else if (GestureManager.CurGesture == GestureManager.Gesture.Tap
        //        && _exit.Collision.IsPointInside(ref GestureManager.CurTouchPoint)) 
        //    {
        //        _exit.PlaySound();
        //        GameProperties.Vibrate(.05);
        //        this.Destroy();
        //        GameProperties.ExitGame = true;
        //    }
        //    else if (GestureManager.CurGesture == GestureManager.Gesture.Tap
        //       && _settings.Collision.IsPointInside(ref GestureManager.CurTouchPoint))
        //    {
        //        _settings.PlaySound();
        //        this.MoveToScreen(typeof(Screens.SettingsScreen).FullName);
        //        GameProperties.Vibrate(.05);
        //    }    
        //}

        private void HandleGestures()
        {
            foreach (GameGesture g in GameGestureManager.Gestures)
            {
                Vector3 gestureEnd = g.End;
                // level select button collision
                if (g.GameGestureType == GameGestureType.Tap
                    && _levelSelect.Collision.IsPointInside(ref gestureEnd))
                {
                    _levelSelect.PlaySound();
                    this.Move(typeof(Screens.WorldSelectScreen).FullName);
                    GameProperties.Vibrate(.05);
                    return;
                }
                // exit button collision
                else if (g.GameGestureType == GameGestureType.Tap
                    && _exit.Collision.IsPointInside(ref gestureEnd))
                {
                    _exit.PlaySound();
                    GameProperties.Vibrate(.05);
                    this.Destroy();
                    GameProperties.ExitGame = true;
                    return;
                }
                else if (g.GameGestureType == GameGestureType.Tap
                   && _settings.Collision.IsPointInside(ref gestureEnd))
                {
                    _settings.PlaySound();
                    this.Move(typeof(Screens.SettingsScreen).FullName);
                    GameProperties.Vibrate(.05);
                    return;
                }
            }
        }

        #endregion


        #endregion
    }
}

