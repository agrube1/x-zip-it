using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Graphics;

using Xzipit.GameObjects;
using Xzipit.MenuObjects;
using Xzipit.Utilities;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Math.Geometry;
using Xzipit.Utilities.Gestures;
using Xzipit.Utilities.Touch;
 
namespace Xzipit.Screens
{
    public class SettingsScreen : Screen
    {
        private CheckBox _vib;
        private CheckBox _sound;
        private Sprite _title;
        private Sprite _bg;

        private MenuButton _credits;
        private MenuButton _delete;

        private ConfirmationBox _confirm;

        private enum SettingsState
        {
            Normal,
            Delete
        }

        private SettingsState _state;

        #region Methods

        #region Constructor and Initialize

        public SettingsScreen()
            : base("MenuScreen")
        {
            // Don't put initialization code here, do it in
            // the Initialize method below
            //   |   |   |   |   |   |   |   |   |   |   |
            //   |   |   |   |   |   |   |   |   |   |   |
            //   V   V   V   V   V   V   V   V   V   V   V

        }

        public override void Initialize(bool addToManagers)
        {
            // Put menu buttons here.
            // Menu buttons should include level select, directions, credits, and exit. 

            _credits = new MenuButton("Global", "credits");
            _delete = new MenuButton("Global", "delete");
            _confirm = new ConfirmationBox("Global", "DELETE!");

            _state = SettingsState.Normal;

            // AddToManagers should be called LAST in this method:
            if (addToManagers)
            {
                AddToManagers();
            }
        }

        public override void AddToManagers()
        {
            string contentManagerName = "Global";

            _bg = SpriteManager.AddSprite(@"Content\Backgrounds\menubg", contentManagerName);
            _bg.Z = -0.1f;
            _bg.PixelScale();
            _bg.RotationZ = (float)Math.PI / 2;

            _title = SpriteManager.AddSprite(@"Content\MenuObjects/Settings/title", contentManagerName);
            _title.Y = 14.0f;
            _title.PixelScale();

            _vib = new CheckBox(contentManagerName, @"Content/MenuObjects/Settings/vibrations");
            _vib.Y = 9.0f;
            _vib.Enabled = GameProperties.Vibration;
            
            _sound = new CheckBox(contentManagerName, @"Content/MenuObjects/Settings/sounds");
            _sound.Y = 4.0f;
            _sound.Enabled = GameProperties.Sound;
            
            _credits.Position.Y = -4.5f;
            _delete.Position.Y = -10.5f;
            _confirm.Y = 6.0f;

        }       

        #endregion

        #region Public Methods

        public override void Activity(bool firstTimeCalled)
        {
            if (!firstTimeCalled)
            {
                // Check for input against buttons here.
                //GestureManager.Update(0.0f);
                //HandleCollisions();

                GameGestureManager.Update();
                HandleGestures();


                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed && !firstTimeCalled)
                {
                    if (_state == SettingsState.Normal)
                    {
                        this.Move(typeof(Screens.MenuScreen).FullName);
                    }
                    else
                    {
                        _confirm.FadeOut();
                        _state = SettingsState.Normal;
                    }
                }
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            _credits.Destroy();
            _delete.Destroy();
            _vib.Destroy();
            _sound.Destroy();
            _confirm.Destroy();
            SpriteManager.RemoveSprite(_title);
            SpriteManager.RemoveSprite(_bg);
        }

        private void HandleGestures()
        {
            foreach (GameGesture g in GameGestureManager.Gestures)
            {
                Vector3 gestureEnd = g.End;

                if (_state == SettingsState.Normal)
                {
                    // vibration checkbox collision
                    if (g.GameGestureType == GameGestureType.Tap
                        && _vib.Collision.IsPointInside(ref gestureEnd))
                    {
                        _vib.Enabled = !_vib.Enabled;
                        GameProperties.Vibration = _vib.Enabled;
                        GameProperties.Vibrate(.05);
                        _vib.PlaySound();

                        GameProperties.SaveSettings();
                    }
                    // sound checkbox collision
                    else if (g.GameGestureType == GameGestureType.Tap
                        && _sound.Collision.IsPointInside(ref gestureEnd))
                    {
                        _sound.Enabled = !_sound.Enabled;
                        GameProperties.Sound = _sound.Enabled;
                        GameProperties.Vibrate(.05);
                        _sound.PlaySound();

                        if (_sound.Enabled)
                        {
                            Microsoft.Xna.Framework.Media.MediaPlayer.Play(GameProperties.BgMusic);
                        }
                        else
                        {
                            Microsoft.Xna.Framework.Media.MediaPlayer.Stop();
                        }

                        GameProperties.SaveSettings();
                    }
                    // credits button collision
                    else if (g.GameGestureType == GameGestureType.Tap
                        && _credits.Collision.IsPointInside(ref gestureEnd))
                    {
                        _credits.PlaySound();
                        this.Move(typeof(Screens.CreditScreen).FullName);
                        GameProperties.Vibrate(.05);
                    }
                    // delete button collision
                    else if (g.GameGestureType == GameGestureType.Tap
                        && _delete.Collision.IsPointInside(ref gestureEnd))
                    {
                        _delete.PlaySound();
                        GameProperties.Vibrate(.05);

                        _confirm.FadeIn();
                        _state = SettingsState.Delete;
                    }
                }
                else
                {
                    _confirm.Activity();
                    if (_confirm.Yes)
                    {
                        // SECRET RESTART BUTTON
                        GameProperties.Save.WipeData();

                        _confirm.FadeOut();
                        _state = SettingsState.Normal;
                        _confirm.Yes = false;
                    }
                    else if (_confirm.No)
                    {
                        _confirm.FadeOut();
                        _state = SettingsState.Normal;
                        _confirm.No = false;
                    }
                }
            }

        }

        #endregion

        #endregion
    }
}

