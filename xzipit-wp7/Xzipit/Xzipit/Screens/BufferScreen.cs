using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Graphics;

using Xzipit.GameObjects;
using Xzipit.MenuObjects;
using Xzipit.Utilities;

using Microsoft.Devices;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Xzipit.Screens
{
    public class BufferScreen : Screen
    {

        #region Methods

        #region Constructor and Initialize

        public BufferScreen()
            : base("LevelSelectScreen")
        {
            // Don't put initialization code here, do it in
            // the Initialize method below
            //   |   |   |   |   |   |   |   |   |   |   |
            //   |   |   |   |   |   |   |   |   |   |   |
            //   V   V   V   V   V   V   V   V   V   V   V

        }

        public override void Initialize(bool addToManagers)
        {

            // AddToManagers should be called LAST in this method:
            if (addToManagers)
            {
                AddToManagers();
            }

        }

        public override void AddToManagers()
        {

        }

        #endregion

        #region Public Methods

        public override void Activity(bool firstTimeCalled)
        {
            if (!firstTimeCalled)
            {
                //GameProperties.SelectedLevel++;
                //GameProperties.DoneLevel = false;
                //LevelManager.LoadInfo();
                //LevelManager.WipeScreen();
                //LevelManager.Restarting = false;
                //this.Move(typeof(Screens.GameScreen).FullName);
            }

        }

        public override void Destroy()
        {
            base.Destroy();
         }

        #endregion


        #endregion
    }
}

