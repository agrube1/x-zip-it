using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Graphics;

using Xzipit.GameObjects;
using Xzipit.MenuObjects;
using Xzipit.Utilities;

using Microsoft.Devices;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using FlatRedBall.Math.Geometry;
using Xzipit.Utilities.Gestures;
using System.Diagnostics;

namespace Xzipit.Screens
{
    public class WorldSelectScreen : Screen
    {
        private WorldButtonList _buttons;
        private Sprite _logo;
        private Sprite _bg;

        #region Constants
        
        private const int NUM_WORLDS = 4;
        #endregion

        #region Methods

        #region Constructor and Initialize

        public WorldSelectScreen()
            : base("WorldSelectScreen")
        {
            // Don't put initialization code here, do it in
            // the Initialize method below
            //   |   |   |   |   |   |   |   |   |   |   |
            //   |   |   |   |   |   |   |   |   |   |   |
            //   V   V   V   V   V   V   V   V   V   V   V

        }

        public override void Initialize(bool addToManagers)
        {

            // AddToManagers should be called LAST in this method:
            if (addToManagers)
            {
                AddToManagers();
            }
        }

        public override void AddToManagers()
        {
            string contentManagerName = "Global";
            _logo = SpriteManager.AddSprite(@"Content\MenuObjects\worldSelect", contentManagerName);
            _logo.X = 0.0f;
            _logo.Y = 14.0f;
            float pixelsPerUnit = SpriteManager.Camera.PixelsPerUnitAt(_logo.Z);
            _logo.ScaleX = .6f * _logo.Texture.Width / pixelsPerUnit;
            _logo.ScaleY = .6f * _logo.Texture.Height / pixelsPerUnit;

            _bg = SpriteManager.AddSprite(@"Content\Backgrounds\menubg", "Global");
            _bg.Z = -0.1f;
            _bg.ScaleX = .5f * _bg.Texture.Width / pixelsPerUnit;
            _bg.ScaleY = .5f * _bg.Texture.Height / pixelsPerUnit;
            _bg.RotationZ = (float)Math.PI / 2;

            _buttons = new WorldButtonList("Global", 4);
            _buttons.MoveToButton(GameProperties.SelectedWorld);
            LoadAllWorldRecords();
        }

        #endregion

        #region Public Methods

        public override void Activity(bool firstTimeCalled)
        {
            if (!firstTimeCalled)
            {
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                {
                    this.Move(typeof(Screens.MenuScreen).FullName);
                }

                GameGestureManager.Update();

                _buttons.Activity();

                if (_buttons.TransitionReady)
                {
                    this.Move(typeof(Screens.LevelSelectScreen).FullName);
                }

                //GestureManager.Update(0.0f);

                //HandleCollisions();
            }
            
        }

        public override void Destroy()
        {
            base.Destroy();
            SpriteManager.RemoveSprite(_logo);
            SpriteManager.RemoveSprite(_bg);

            _buttons.Destroy();
        }

        private void HandleCollisions()
        {
            
        }

        #endregion

        #region Private Initialization Methods

        /// <summary>
        /// Load whether the level is locked or unlocked
        /// </summary>
        private void LoadAllWorldRecords()
        {

        }

        #endregion

        #region Private Methods

        #endregion

        #endregion
    }
}

