using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Graphics;

using Xzipit.GameObjects;
using Xzipit.MenuObjects;
using Xzipit.Utilities;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
 
namespace Xzipit.Screens
{
    public class CreditScreen : Screen
    {
        private List<Text> _credits;
        private Sprite _bg;
        private Sprite _frb;
        private Sprite _gdc;
        private Sprite _nng;

        #region Methods

        #region Constructor and Initialize

        public CreditScreen()
            : base("MenuScreen")
        {
            // Don't put initialization code here, do it in
            // the Initialize method below
            //   |   |   |   |   |   |   |   |   |   |   |
            //   |   |   |   |   |   |   |   |   |   |   |
            //   V   V   V   V   V   V   V   V   V   V   V

        }

        public override void Initialize(bool addToManagers)
        {
            // AddToManagers should be called LAST in this method:
            if (addToManagers)
            {
                AddToManagers();
            }
        }

        public override void AddToManagers()
        {
            string contentManagerName = "Global";

            _credits = new List<Text>();

            _credits.Add(TextManager.AddText("Lead and Programmer", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Exander Grube", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Programmer", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Nathaniel Lam", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Artist", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Eve Addison", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Music", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Michael Winch", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Sound", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Nick Beatty", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Video", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("David Kim", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Kalada Jumbo", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Brandon Moye", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("David Mai", GameProperties.CustomFont));
            _credits.Add(TextManager.AddText("Jonathan Moriarty", GameProperties.CustomFont));
            
            for (int i = 0; i < 12; i++)
            {
                _credits[i].Scale = 0.8f;
                _credits[i].Spacing = 0.8f;
                _credits[i].SetColor(0.0f, 0.0f, 0.0f);
                _credits[i].X = -7.0f;
                _credits[i].Y = 15f - (i * 1.5f + (i / 2) * 0.5f);
            }

            for (int i = 12; i < 16; i++)
            {
                _credits[i].Scale = 0.6f;
                _credits[i].Spacing = 0.6f;
                _credits[i].SetColor(0.0f, 0.0f, 0.0f);
                _credits[i].X = -8.0f;
                _credits[i].Y = -10.0f - ((i - 12) * 1.5f);
            }

            _bg = SpriteManager.AddSprite(@"Content\Backgrounds\menubg", "Global");
            _bg.Z = -0.1f;
            float pixelsPerUnit = SpriteManager.Camera.PixelsPerUnitAt(_bg.Z);
            _bg.ScaleX = .5f * _bg.Texture.Width / pixelsPerUnit;
            _bg.ScaleY = .5f * _bg.Texture.Height / pixelsPerUnit;
            _bg.RotationZ = (float)Math.PI / 2;

            _frb = SpriteManager.AddSprite(@"Content\frb-logo", "Global");
            _frb.PixelScale();
            _frb.ScaleX *= 0.3f;
            _frb.ScaleY *= 0.3f;
            _frb.X = 4.5f;
            _frb.Y = -7.5f;

            _gdc = SpriteManager.AddSprite(@"Content\gdc-logo", "Global");
            _gdc.PixelScale();
            _gdc.ScaleX *= 0.3f;
            _gdc.ScaleY *= 0.3f;
            _gdc.X = -4.5f;
            _gdc.Y = -7.5f;

            _nng = SpriteManager.AddSprite(@"Content\nng-logo-small", "Global");
            _nng.PixelScale();
            _nng.ScaleX *= 0.4f;
            _nng.ScaleY *= 0.4f;
            _nng.X = 4.5f;
            _nng.Y = -12.5f;
            
        }

        #endregion

        #region Public Methods

        public override void Activity(bool firstTimeCalled)
        {
            if (!firstTimeCalled)
            {
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                {
                    this.Move(typeof(Screens.SettingsScreen).FullName);
                }
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            SpriteManager.RemoveSprite(_bg);
            SpriteManager.RemoveSprite(_frb);
            SpriteManager.RemoveSprite(_gdc);
            SpriteManager.RemoveSprite(_nng);
            foreach (Text t in _credits)
            {
                TextManager.RemoveText(t);
            }
        }


        #endregion

       

        #endregion
    }
}

