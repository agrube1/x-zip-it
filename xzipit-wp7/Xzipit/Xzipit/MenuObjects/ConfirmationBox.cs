
using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework.Audio;
using Xzipit.Utilities;
using Xzipit.Utilities.Gestures;

namespace Xzipit.MenuObjects
{
    class ConfirmationBox : MenuObject
    {
        #region Fields

        private Text _title;
        private string _titleString;

        private BoxButton _yes;
        private BoxButton _no;

        // Keep the ContentManager for easy access:
        string _contentManagerName;

        #endregion

        #region Properties
        // All properties of Button are already covered in Obstacle.
        public bool Visible
        {
            get { return _sprite.Visible; }
            set
            {
                _sprite.Visible = value;
                _title.Visible = value;
            }
        }

        public override float AlphaRate
        {
            get { return _sprite.AlphaRate; }
            set
            {
                _sprite.AlphaRate = value;
                _title.AlphaRate = value;
                _yes.AlphaRate = value;
                _no.AlphaRate = value;
            }
        }

        public override float Alpha
        {
            get { return _sprite.Alpha; }
            set
            {
                _sprite.Alpha = value;
                _title.Alpha = value;
                _yes.Alpha = value;
                _no.Alpha = value;
            }
        }

        public bool Fading
        {
            get;
            set;
        }

        public bool Yes
        {
            get;
            set;
        }

        public bool No
        {
            get;
            set;
        }

        #endregion

        #region Methods

        #region Constructor
        public ConfirmationBox(string contentManagerName, string title)
        {
            _titleString = title;
            // Set the ContentManagerName and call Initialize:
            _contentManagerName = contentManagerName;

            // If you don't want to add to managers, make an overriding constructor
            Initialize(true);
        }

        #endregion

        #region Initialization
        protected override void Initialize(bool addToManagers)
        {
            // Here you can preload any content you will be using
            // like .scnx files or texture files.

            if (addToManagers)
            {
                AddToManagers(null);
            }
        }

        public override void AddToManagers(Layer layerToAddTo)
        {
            // Add the Entity to the SpriteManager
            // so it gets managed properly (velocity, acceleration, attachments, etc.)
            SpriteManager.AddPositionedObject(this);

            _sprite = SpriteManager.AddSprite(@"Content\MenuObjects\PauseBox\top", _contentManagerName);
            _sprite.AttachTo(this, false);
            _sprite.PixelScale();
            
            _title = TextManager.AddText(_titleString, GameProperties.CustomFont);
            _title.Scale = 1.3f;
            _title.Spacing = 1.3f;
            _title.AttachTo(_sprite, false);
            _title.SetColor(0.0f, 0.0f, 0.0f);
            _title.RelativeX -= _title.HorizontalCenter;
            _title.RelativeY = -0.25f;
            _title.RelativeZ = 0.05f;

            _yes = new BoxButton("Yes");
            _yes.RelativeY = -4.0f;
            _yes.AttachTo(this, false);
            _yes.RelativeZ = 0.05f;
            _no = new BoxButton("No");
            _no.AttachTo(this, false);
            _no.RelativeY = -8.0f;
            _no.RelativeZ = 0.05f;
            
            _enabled = true;

            _touchSound = FlatRedBallServices.Load<SoundEffect>(@"Content\Sound\button");

            Fading = false;
            Visible = true;
            Alpha = 0.0f;

            Yes = false;
            No = false;
        }

        #endregion

        #region Update
        public override void Activity()
        {
            CheckFadingStatus();
            foreach (GameGesture g in GameGestureManager.Gestures)
            {
                if (_yes.WasTapped(g))
                {
                    PlaySound();
                    GameProperties.Vibrate(.05);
                    Yes = true;
                    break;
                }
                else if (_no.WasTapped(g))
                {
                    PlaySound();
                    GameProperties.Vibrate(.05);
                    No = true;
                    break;
                }
            }
        }

        #endregion

        #region Destroy

        public override void Destroy()
        {
            TextManager.RemoveText(_title);

            _yes.Destroy();
            _no.Destroy();

            base.Destroy();
        }

        public void Reset()
        {
            Yes = false;
            No = false;
        }

        #endregion

        #region Fading
        public virtual void FadeOut()
        {
            this.Fading = true;
            this.AlphaRate = -GameProperties.ALPHA_RATE;
        }

        public virtual void FadeIn()
        {
            this.Fading = true;
            this.AlphaRate = GameProperties.ALPHA_RATE;
        }

        public virtual bool CheckFadingStatus()
        {
            if (this.Alpha >= 1.0f && this.AlphaRate == GameProperties.ALPHA_RATE)
            {
                this.Fading = false;

                this.Alpha = 1.0f;
                this.AlphaRate = 0.0f;

                return true;
            }
            else if (this.Alpha <= 0.0f && this.AlphaRate == -GameProperties.ALPHA_RATE)
            {
                this.Fading = false;

                this.Alpha = 0.0f;
                this.AlphaRate = 0.0f;

                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Private Methods

        #endregion

        #endregion
    }
}
