
using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Math.Geometry;

using Microsoft.Xna.Framework.Audio;

using Xzipit.Utilities;

using Xzipit.Interfaces;
using Xzipit.Utilities.Gestures;
using Microsoft.Xna.Framework;

namespace Xzipit.MenuObjects
{
    class RestartButton : MenuObject
    {
        #region Fields

        // Keep the ContentManager for easy access:
        string _contentManagerName;

        #endregion


        #region Methods

        // Constructor
        public RestartButton(string contentManagerName)
        {
            // Set the ContentManagerName and call Initialize:
            _contentManagerName = contentManagerName;

            // If you don't want to add to managers, make an overriding constructor
            Initialize(true);
        }

        protected override void Initialize(bool addToManagers)
        {
            // Here you can preload any content you will be using
            // like .scnx files or texture files.

            if (addToManagers)
            {
                AddToManagers(null);
            }
        }

        public override void AddToManagers(Layer layerToAddTo)
        {
            // Add the Entity to the SpriteManager
            // so it gets managed properly (velocity, acceleration, attachments, etc.)
            SpriteManager.AddPositionedObject(this);

            // Here you may want to add your objects to the engine.  Use layerToAddTo
            // when adding if your Entity supports layers.  Make sure to attach things
            // to this if appropriate.
            _sprite = SpriteManager.AddSprite(@"Content\MenuObjects\restart");
            _sprite.AttachTo(this, false);

            _sprite.PixelScale();
            _sprite.ScaleX *= 1.18f;
            _sprite.ScaleY *= 1.18f;

            //Polygon 
            this._collision = ShapeManager.AddAxisAlignedRectangle();
            this._collision.AttachTo(this, false);
            this._collision.ScaleX = 1.0f * _sprite.ScaleX;
            this._collision.ScaleY = 1.0f * _sprite.ScaleY;
            this._collision.Visible = false;

            _enabled = true;
            _touchSound = FlatRedBallServices.Load<SoundEffect>(@"Content\Sound\button");
        }

        public override void Activity()
        {
            
        }

        public bool Collide()
        {
            foreach (GameGesture g in GameGestureManager.Gestures)
            {
                if (g.GameGestureType == GameGestureType.Tap)
                {
                    Vector3 gestureEnd = g.End;

                    if (Collision.IsPointInside(ref gestureEnd))
                    {
                        GameProperties.Vibrate(.05);
                        PlaySound();
                        return true;
                    }
                }
            }

            return false;
        }

        public virtual void Destroy()
        {
            base.Destroy();
        }

        #endregion
    }
}
