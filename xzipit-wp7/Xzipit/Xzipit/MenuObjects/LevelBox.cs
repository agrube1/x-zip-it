
using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework.Audio;
using Xzipit.Utilities;
using System.Globalization;

namespace Xzipit.MenuObjects
{
    class LevelBox : MenuObject
    {
        #region Fields

        private Text _name;
        private Text _description;
        private Text _target;
        private Text _target2;
        private Text _target3;
        private Sprite _reward;
        private Sprite _title;
        private Sprite _accept;

        // Keep the ContentManager for easy access:
        string _contentManagerName;

        #endregion

        #region Properties
        // All properties of Button are already covered in Obstacle.
        public bool Visible
        {
            get { return _sprite.Visible; }
            set
            {
                _sprite.Visible = value;
                _title.Visible = value;
                _name.Visible = value;
                _description.Visible = value;
                _target.Visible = value;
                _target2.Visible = value;
                _target3.Visible = value;
                _reward.Visible = value;
                _accept.Visible = value;
            }
        }

        public float AlphaRate
        {
            get { return _sprite.AlphaRate; }
            set
            {
                _sprite.AlphaRate = value;
                _title.AlphaRate = value;
                _name.AlphaRate = value;
                _description.AlphaRate = value;
                _target.AlphaRate = value;
                _target2.AlphaRate = value;
                _target3.AlphaRate = value;
                _reward.AlphaRate = value;
                _accept.AlphaRate = value;
            }
        }

        public float Alpha
        {
            get { return _sprite.Alpha; }
            set
            {
                _sprite.Alpha = value;
                _title.Alpha = value;
                _name.Alpha = value;
                _description.Alpha = value;
                _target.Alpha = value;
                _target2.Alpha = value;
                _target3.Alpha = value;
                _reward.Alpha = value;
                _accept.Alpha = value;
            }
        }

        public bool Fading
        {
            get;
            set;
        }
        #endregion

        #region Methods

        #region Constructor
        public LevelBox(string contentManagerName)
        {
            // Set the ContentManagerName and call Initialize:
            _contentManagerName = contentManagerName;

            // If you don't want to add to managers, make an overriding constructor
            Initialize(true);
        }

        #endregion

        #region Initialization
        protected override void Initialize(bool addToManagers)
        {
            // Here you can preload any content you will be using
            // like .scnx files or texture files.

            if (addToManagers)
            {
                AddToManagers(null);
            }
        }

        public override void AddToManagers(Layer layerToAddTo)
        {
            // Add the Entity to the SpriteManager
            // so it gets managed properly (velocity, acceleration, attachments, etc.)
            SpriteManager.AddPositionedObject(this);

            _title = SpriteManager.AddSprite(@"Content\MenuObjects\LevelBox\title", _contentManagerName);
            _title.AttachTo(this, false);
            _title.PixelScale();

            _sprite = SpriteManager.AddSprite(@"Content\MenuObjects\LevelBox\descrip", _contentManagerName);
            _sprite.AttachTo(this, false);
            _sprite.RelativeY = -8.0f;
            _sprite.PixelScale();

            _name = TextManager.AddText("", GameProperties.CustomFont);
            _name.Scale = 1.0f;
            _name.Spacing = 1.1f;
            _name.AttachTo(_title, false);
            _name.SetColor(0.0f, 0.0f, 0.0f);
            _name.RelativeX = -5.0f;
            _name.RelativeY = 0.5f;

            _description = TextManager.AddText("", GameProperties.CustomFont);
            _description.Scale = 0.5f;
            _description.Spacing = 0.6f;
            _description.SetColor(0.0f, 0.0f, 0.0f);
            _description.AttachTo(_title, false);
            _description.RelativeX = -5.0f;
            _description.RelativeY = -0.7f;

            _target = TextManager.AddText("", GameProperties.CustomFont);
            _target.Scale = GameProperties.TIME_SCALE;
            _target.Spacing = GameProperties.TIME_SCALE;
            _target.SetColor(0.0f, 0.0f, 0.0f);
            _target.AttachTo(_sprite, false);
            _target.RelativeX = -5.0f;
            _target.RelativeY = 2.0f;

            _target2 = TextManager.AddText("", GameProperties.CustomFont);
            _target2.Scale = GameProperties.TIME_SCALE;
            _target2.Spacing = GameProperties.TIME_SCALE;
            _target2.SetColor(0.0f, 0.0f, 0.0f);
            _target2.AttachTo(_sprite, false);
            _target2.RelativeX = -5.0f;
            _target2.RelativeY = 0.0f;

            _target3 = TextManager.AddText("", GameProperties.CustomFont);
            _target3.Scale = GameProperties.TIME_SCALE;
            _target3.Spacing = GameProperties.TIME_SCALE;
            _target3.SetColor(0.0f, 0.0f, 0.0f);
            _target3.AttachTo(_sprite, false);
            _target3.RelativeX = -5.0f;
            _target3.RelativeY = -2.0f;

            _reward = SpriteManager.AddSprite(GameProperties.GetPins());
            _reward.AttachTo(_title, false);
            _reward.Animate = false;
            _reward.PixelScale();
            _reward.CurrentFrameIndex = 0;
            _reward.RelativeX = 0.0f;
            _reward.RelativeY = 2.0f;
            _reward.UseAnimationRelativePosition = false;

            _accept = SpriteManager.AddSprite(@"Content\MenuObjects\zipPlay", _contentManagerName);
            _accept.AttachTo(_sprite, false);
            _accept.PixelScale();
            _accept.RelativeX = 0.0f;
            _accept.RelativeY = -7.5f;

            this._collision = ShapeManager.AddAxisAlignedRectangle();
            this._collision.AttachTo(_accept, false);
            this._collision.ScaleX = _accept.ScaleX * 1.6f;
            this._collision.ScaleY = _accept.ScaleY * 1.6f;
            this._collision.Visible = false;

            _enabled = true;

            _touchSound = FlatRedBallServices.Load<SoundEffect>(@"Content\Sound\button");

            Fading = false;
            Visible = true;
            Alpha = 0.0f;
        }

        #endregion

        #region Update
        public override void Activity()
        {
            // This code should do things like set Animations, respond to input, and so on.
        }

        #endregion

        #region Destroy

        public virtual void Destroy()
        {
            base.Destroy();

            TextManager.RemoveText(_name);
            TextManager.RemoveText(_description);
            TextManager.RemoveText(_target);
            TextManager.RemoveText(_target2);
            TextManager.RemoveText(_target3);
            SpriteManager.RemoveSprite(_reward);
            SpriteManager.RemoveSprite(_accept);
            SpriteManager.RemoveSprite(_title);
        }

        #endregion

        #region Fading
        public virtual void FadeOut()
        {
            this.Fading = true;
            this.AlphaRate = -GameProperties.ALPHA_RATE;
        }

        public virtual void FadeIn(string levelBoxInfo)
        {
            this.Fading = true;
            this.AlphaRate = GameProperties.ALPHA_RATE;

            SetInfo(levelBoxInfo);
        }

        public virtual bool CheckFadingStatus()
        {
            if (this.Alpha >= 1.0f && this.AlphaRate == GameProperties.ALPHA_RATE)
            {
                this.Fading = false;

                this.Alpha = 1.0f;
                this.AlphaRate = 0.0f;

                return true;
            }
            else if (this.Alpha <= 0.0f && this.AlphaRate == -GameProperties.ALPHA_RATE)
            {
                this.Fading = false;

                this.Alpha = 0.0f;
                this.AlphaRate = 0.0f;

                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Private Methods

        #region Set
        private void SetInfo(string levelBoxInfo)
        {
            string[] split = levelBoxInfo.Trim().Split('|');
            _name.DisplayText = split[0];
            _description.DisplayText = split[1];
            _target.DisplayText = "Gold: " + double.Parse(split[2], CultureInfo.InvariantCulture).ToTimeString();
            _target2.DisplayText = "Silver: " + double.Parse(split[3], CultureInfo.InvariantCulture).ToTimeString();
            _target3.DisplayText = "Bronze: " + double.Parse(split[4], CultureInfo.InvariantCulture).ToTimeString();

            SetPinVisible();
        }

        private void SetPinVisible()
        {
            if (GameProperties.LevelPin > 3)
            {
                _reward.Visible = false;
            }
            else
            {
                _reward.Visible = true;
                _reward.CurrentFrameIndex = GameProperties.LevelPin;
            }
        }
        #endregion

        #endregion

        #endregion
    }
}
