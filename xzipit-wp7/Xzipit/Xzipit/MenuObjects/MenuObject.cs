using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Math.Geometry;

using Microsoft.Xna.Framework.Audio;
using Xzipit.Utilities;

namespace Xzipit.MenuObjects
{
    public abstract class MenuObject : PositionedObject
    {
        protected AxisAlignedRectangle _collision;
        protected Sprite _sprite;
        protected bool _enabled;
        protected SoundEffect _touchSound;


        public AxisAlignedRectangle Collision
        {
            get { return _collision; }
        }

        public bool Enabled
        {
            get { return _enabled; }
        }

        public virtual float Alpha
        {
            get { return _sprite.Alpha; }
            set { _sprite.Alpha = value; }
        }

        public virtual float AlphaRate
        {
            get { return _sprite.AlphaRate; }
            set { _sprite.AlphaRate = value; }
        }

        protected abstract void Initialize(bool addToManagers);

        public abstract void AddToManagers(Layer layerToAddTo);

        public abstract void Activity();

        public virtual void Destroy()
        {
            // Remove self from the SpriteManager:
            SpriteManager.RemovePositionedObject(this);
            SpriteManager.RemoveSprite(_sprite);

            // Remove any other objects you've created:
            if (_collision != null)
                ShapeManager.Remove(_collision);
        }

        public virtual void PlaySound()
        {
            _touchSound.PlaySound();
        }

    }
}
