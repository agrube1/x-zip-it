using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Input;

using Microsoft.Xna.Framework;

using Xzipit.Utilities;

namespace Xzipit.MenuObjects
{
    class TimeBox : MenuObject
    {
        #region Fields

        private Text _time;
        private Sprite _pin;
        private int _curPin;

        private bool _pause;

        public bool Visible;

        // Keep the ContentManager for easy access:
        string _contentManagerName;

        #endregion

        #region Properties
        // All properties of Button are already covered in Obstacle.
        public double Time
        {
            set { _time.DisplayText = value.ToTimeString(); }
        }

        #endregion

        #region Methods

        // Constructor
        public TimeBox(string contentManagerName)
        {
            // Set the ContentManagerName and call Initialize:
            _contentManagerName = contentManagerName;

            // If you don't want to add to managers, make an overriding constructor
            Initialize(true);
        }

        protected override void Initialize(bool addToManagers)
        {
            // Here you can preload any content you will be using
            // like .scnx files or texture files.
            Visible = false;
            _curPin = 0;

            if (addToManagers)
            {
                AddToManagers(null);
            }
        }

        public override void AddToManagers(Layer layerToAddTo)
        {
            // Add the Entity to the SpriteManager
            // so it gets managed properly (velocity, acceleration, attachments, etc.)
            SpriteManager.AddPositionedObject(this);

            _sprite = SpriteManager.AddSprite(@"Content\MenuObjects\timeBox", _contentManagerName);
            _sprite.AttachTo(this, false);

            _sprite.PixelScale();
            _sprite.ScaleX *= .65f;
            _sprite.ScaleY *= .60f;

            _time = TextManager.AddText("", GameProperties.CustomFont);
            _time.Scale = GameProperties.TIME_SCALE;
            _time.Spacing = GameProperties.TIME_SCALE;
            _time.SetColor(0.0f, 0.0f, 0.0f);
            _time.AttachTo(this, false);
            _time.RelativeX = -2.1f;
            _time.RelativeY = 0.0f;
            _time.RelativeZ = 0.1f;
            _time.DisplayText = ((double)0).ToTimeString();

            _pin = SpriteManager.AddSprite(GameProperties.GetPins());
            _pin.AttachTo(this, false);
            _pin.Animate = false;
            _pin.RelativeX = -2.9f;
            _pin.RelativeZ = 0.1f;
            _pin.CurrentFrameIndex = 0;
            _pin.UseAnimationRelativePosition = false;
            _pin.Visible = false;

            _enabled = true;
        }

        public override void Activity()
        {
            // This code should do things like set Animations, respond to input, and so on.
            UpdateTime(0);
            UpdatePin(0);
        }

        public virtual void Activity(double curTime, List<double> targetTimes)
        {
            UpdateTime(curTime);
            UpdatePin(curTime, targetTimes);
        }

        public override void Destroy()
        {
            base.Destroy();

            TextManager.RemoveText(_time);
            SpriteManager.RemoveSprite(_pin);
        }

        // changes the time counter every frame in the time box.
        private void UpdateTime(double curTime)
        {
            _time.DisplayText = curTime.ToTimeString();
        }

        // changes the pin color in the time box.
        private void UpdatePin(double curTime)
        {
            _pin.Visible = false;
            _curPin = 0;
        }

        public void Reset()
        {
            _time.DisplayText = ((double)0.0).ToTimeString();
            _pin.Visible = false;
            _curPin = 0;
        }

        // changes the pin color in the time box.
        public void UpdatePin(double curTime, List<double> targetTimes)
        {
            _pin.Visible = true;
            if ((_curPin < 3) && (curTime > targetTimes[_curPin]))
            {
                _curPin++;
            }
            if ((_curPin >= 0) && (_curPin <= 3))
            {
                _pin.CurrentFrameIndex = _curPin;
            }
        }

        public void FlipPausedState()
        {
            _pause = !_pause;
        }

        #endregion
    }
}
