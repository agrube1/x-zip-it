using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework.Audio;
using FlatRedBall.Graphics.Animation;

using Xzipit.Utilities;

namespace Xzipit.MenuObjects
{
    class CheckBox : MenuObject
    {
        #region Fields

        private string _filename;

        // Keep the ContentManager for easy access:
        string _contentManagerName;

        #endregion

        #region Properties
        // All properties of Button are already covered in Obstacle.
        public bool Enabled
        {
            get { return _enabled; }
            set { 
                _enabled = value;
                _sprite.CurrentFrameIndex = _enabled ? 0 : 1;
            }
        }
        #endregion

        #region Methods

        // Constructor
        public CheckBox(string contentManagerName, string filename)
        {
            // Set the ContentManagerName and call Initialize:
            _contentManagerName = contentManagerName;
            _filename = filename;

            // If you don't want to add to managers, make an overriding constructor
            Initialize(true);
        }

        protected override void Initialize(bool addToManagers)
        {
            // Here you can preload any content you will be using
            // like .scnx files or texture files.

            if (addToManagers)
            {
                AddToManagers(null);
            }
        }

        public override void AddToManagers(Layer layerToAddTo)
        {
            // Add the Entity to the SpriteManager
            // so it gets managed properly (velocity, acceleration, attachments, etc.)
            SpriteManager.AddPositionedObject(this);

            // Here you may want to add your objects to the engine.  Use layerToAddTo
            // when adding if your Entity supports layers.  Make sure to attach things
            // to this if appropriate.
            _sprite = SpriteManager.AddSprite(LoadAnimation());
            _sprite.AttachTo(this, false);
            _sprite.PixelScale();
            _sprite.CurrentFrameIndex = GameProperties.Vibration ? 0 : 1;
            _sprite.Animate = false;

            //Polygon 
            this._collision = ShapeManager.AddAxisAlignedRectangle();
            this._collision.AttachTo(this, false);
            this._collision.ScaleX = 1.4f * _sprite.ScaleX;
            this._collision.ScaleY = 1.4f * _sprite.ScaleY;
            this._collision.Visible = false;

            _enabled = true;

            _touchSound = FlatRedBallServices.Load<SoundEffect>(@"Content\Sound\button");
        }

        public override void Activity()
        {
            // This code should do things like set Animations, respond to input, and so on.
        }

        public virtual void Destroy()
        {
            base.Destroy();
            
        }

        #endregion

        #region Private Methods
        private AnimationChainList LoadAnimation()
        {
            AnimationChainList ani = new AnimationChainList(5);
            AnimationChain anis = new AnimationChain();
            anis.Add(new AnimationFrame(_filename + "Yes", 1.0f, "Global"));
            anis.Add(new AnimationFrame(_filename + "No", 1.0f, "Global"));

            ani.Add(anis);

            return ani;
        }
        #endregion
    }
}
