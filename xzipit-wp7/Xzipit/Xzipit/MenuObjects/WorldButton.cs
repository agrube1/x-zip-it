
using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Math.Geometry;

using Microsoft.Xna.Framework.Audio;

using Xzipit.Utilities;
using FlatRedBall.Graphics.Animation;
using System;
using Microsoft.Xna.Framework;

namespace Xzipit.MenuObjects
{
    class WorldButton : MenuObject
    {
        #region Fields

        private int _world;
        private const int COLL_SIZE = 32;
        private Text _worldName;
        private Sprite _locked;
        private Vector3 _scaleVelocity;

        // Keep the ContentManager for easy access:
        string _contentManagerName;

        #endregion

        #region Properties
        // All properties of Button are already covered in Obstacle.
        public int World
        {
            get { return _world; }
        }

        #endregion

        #region Methods

        // Constructor
        public WorldButton(string contentManagerName, int world)
        {
            // Set the ContentManagerName and call Initialize:
            _contentManagerName = contentManagerName;
            _world = world;

            // If you don't want to add to managers, make an overriding constructor
            Initialize(true);
        }

        protected override void Initialize(bool addToManagers)
        {
            // Here you can preload any content you will be using
            // like .scnx files or texture files.

            if (addToManagers)
            {
                AddToManagers(null);
            }
        }

        public override void AddToManagers(Layer layerToAddTo)
        {
            // Add the Entity to the SpriteManager
            // so it gets managed properly (velocity, acceleration, attachments, etc.)
            SpriteManager.AddPositionedObject(this);

            // Here you may want to add your objects to the engine.  Use layerToAddTo
            // when adding if your Entity supports layers.  Make sure to attach things
            // to this if appropriate.
            _sprite = SpriteManager.AddSprite(LoadThumbnail(), "Global");
            _sprite.AttachTo(this, false);

            _sprite.PixelScale();
            _sprite.ScaleX *= 0.6f;
            _sprite.ScaleY *= 0.6f;
            _sprite.Animate = false;

            float xScaleVelo = _sprite.PixelScaleX() - _sprite.ScaleX;
            float yScaleVelo = _sprite.PixelScaleY() - _sprite.ScaleY;
            _scaleVelocity = new Vector3(xScaleVelo, yScaleVelo, 0.0f);
            _scaleVelocity.Normalize();
            _scaleVelocity *= 20.0f;

            //Polygon 
            this._collision = ShapeManager.AddAxisAlignedRectangle();
            this._collision.AttachTo(this, false);
            this._collision.ScaleX = _sprite.ScaleY;
            this._collision.ScaleY = _sprite.ScaleX;
            this._collision.Visible = false;
            this._collision.RelativeRotationZ = (float)Math.PI / 2;

            _worldName = TextManager.AddText(WorldNumToText(), GameProperties.CustomFont);
            _worldName.AttachTo(this, false);
            _worldName.RelativeY = -13.0f;
            _worldName.RelativeZ = -0.05f;
            _worldName.HorizontalAlignment = HorizontalAlignment.Center;
            //_worldName.RelativeX = -_worldName.HorizontalCenter / 2.75f;
            _worldName.SetColor(0.0f, 0.0f, 0.0f);
            _worldName.Scale = 1.0f;
            _worldName.Spacing = 1.0f;

            _enabled = true;
            
            _touchSound = FlatRedBallServices.Load<SoundEffect>(@"Content\Sound\button");
        }

        public override void Activity()
        {
            // This code should do things like set Animations, respond to input, and so on.
        }

        public virtual void Destroy()
        {
            base.Destroy();

            TextManager.RemoveText(_worldName);
        }

        public bool Enlarge()
        {
            _sprite.ScaleXVelocity = _scaleVelocity.X;
            _sprite.ScaleYVelocity = _scaleVelocity.Y;

            if (_sprite.ScaleX >= _sprite.PixelScaleX())
            {
                _sprite.PixelScale();
                return false;
            }

            return true;
        }

        private string LoadThumbnail()
        {
            return @"Content/Backgrounds/bg" + _world;
        }

        public string WorldNumToText()
        {
            string result = "";

            switch (_world)
            {
                case 1:
                    result = "The Desert";
                    break;
                case 2:
                    result = "The Sea";
                    break;
                case 3:
                    result = "The Mountains";
                    break;
                case 4:
                    result = "The Forest";
                    break;
                default:
                    result = "Whoops";
                    break;
            }

            return result;
        }

        #endregion
    }
}
