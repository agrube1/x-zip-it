using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework.Audio;

namespace Xzipit.MenuObjects
{
    class MenuButton : MenuObject
    {
        #region Fields

        private string _filename;

        // Keep the ContentManager for easy access:
        string _contentManagerName;

        #endregion

        #region Properties
        // All properties of Button are already covered in Obstacle.

        #endregion

        #region Methods

        // Constructor
        public MenuButton(string contentManagerName, string filename)
        {
            // Set the ContentManagerName and call Initialize:
            _contentManagerName = contentManagerName;
            _filename = filename;

            // If you don't want to add to managers, make an overriding constructor
            Initialize(true);
        }

        protected override void Initialize(bool addToManagers)
        {
            // Here you can preload any content you will be using
            // like .scnx files or texture files.

            if (addToManagers)
            {
                AddToManagers(null);
            }
        }

        public override void AddToManagers(Layer layerToAddTo)
        {
            // Add the Entity to the SpriteManager
            // so it gets managed properly (velocity, acceleration, attachments, etc.)
            SpriteManager.AddPositionedObject(this);

            // Here you may want to add your objects to the engine.  Use layerToAddTo
            // when adding if your Entity supports layers.  Make sure to attach things
            // to this if appropriate.
            _sprite = SpriteManager.AddSprite(@"Content\MenuObjects\MenuButtons\" + _filename, _contentManagerName);
            _sprite.AttachTo(this, false);

            float pixelsPerUnit = SpriteManager.Camera.PixelsPerUnitAt(_sprite.Z);
            _sprite.ScaleX = .5f * _sprite.Texture.Width / pixelsPerUnit;
            _sprite.ScaleY = .5f * _sprite.Texture.Height / pixelsPerUnit;

            //Polygon 
            this._collision = ShapeManager.AddAxisAlignedRectangle();
            this._collision.AttachTo(this, false);
            this._collision.ScaleX = _sprite.ScaleX;
            this._collision.ScaleY = _sprite.ScaleY;
            this._collision.Visible = false;

            _enabled = true;

            _touchSound = FlatRedBallServices.Load<SoundEffect>(@"Content\Sound\button");
        }

        public override void Activity()
        {
            // This code should do things like set Animations, respond to input, and so on.
        }

        public virtual void Destroy()
        {
            base.Destroy();
        }

        #endregion
    }
}
