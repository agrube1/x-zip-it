using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlatRedBall.Graphics;
using FlatRedBall;

using Xzipit.Utilities;
using FlatRedBall.Math.Geometry;
using Xzipit.Utilities.Gestures;
using Microsoft.Xna.Framework;

namespace Xzipit.MenuObjects
{
    public class PauseBoxButton : MenuObject
    {
        private Text _text;

        public override float Alpha
        {
            get
            {
                return _sprite.Alpha;
            }
            set
            {
                _sprite.Alpha = value;
                _text.Alpha = value;
            }
        }

        public override float AlphaRate
        {
            get
            {
                return _sprite.AlphaRate;
            }
            set
            {
                _sprite.AlphaRate = value;
                _text.AlphaRate = value;
            }
        }

        public PauseBoxButton(String text)
        {
            Initialize(text);
        }

        public PauseBoxButton()
        {
            Initialize("Resume");
        }

        private void Initialize(String text)
        {
            SpriteManager.AddPositionedObject(this);

            _sprite = SpriteManager.AddSprite(@"Content\MenuObjects\PauseBox\button", "Global");
            _sprite.AttachTo(this, false);
            _sprite.PixelScale();
            _sprite.ScaleX *= .75f;
            _sprite.ScaleY *= .75f;

            _text = TextManager.AddText(text, GameProperties.CustomFont);
            _text.AttachTo(this, false);
            _text.Scale = 1.0f;
            _text.Spacing = 1.0f;
            _text.SetColor(0.0f, 0.0f, 0.0f);
            _text.RelativeX -= _text.HorizontalCenter;
            _text.RelativeY -= _text.VerticalCenter;
            _text.RelativeZ = 0.1f;

            _collision = ShapeManager.AddAxisAlignedRectangle();
            _collision.AttachTo(this, false);
            _collision.ScaleX = _sprite.ScaleX;
            _collision.ScaleY = _sprite.ScaleY;
            _collision.Visible = false;

            Alpha = 0.0f;
        }

        public bool WasTapped(GameGesture g)
        {
            Vector3 endPoint = g.End;

            return (g.GameGestureType == GameGestureType.Tap
                && _collision.IsPointInside(ref endPoint));
        }

        public override void Destroy()
        {
            base.Destroy();

            TextManager.RemoveText(_text);
        }

        protected override void Initialize(bool addToManagers)
        {
            throw new NotImplementedException();
        }

        public override void AddToManagers(Layer layerToAddTo)
        {
            throw new NotImplementedException();
        }

        public override void Activity()
        {
            throw new NotImplementedException();
        }
    }
}
