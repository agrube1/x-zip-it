[::::  Hotkeys for the Editor  ::::]
[:: Mode ::]
i - insert mode
e - edit mode
t - toolbox mode

[:: File ::]
s - saves the level
ctrl + s - save as
o - opens a level
n - opens a new level

[:: Edit ::]
a - rotate object left
d - rotate object right
up - move object up
down - move object down
left - move object left
right - move object right

[:::: Format of the .xlvl File ::::]
l-				---Begin Layer
bg type
t-    			---Begin Track
n x y 			---Node
n x y
...
n x y
-t    	   		---End Track
b x y type x2 y2---Button
v x y type angle---Velcro
k x y type angle---Buckle
o x y type x2 y2---Moving Obstacle
-l				---End Layer